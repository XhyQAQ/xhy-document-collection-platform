package com.xhy.documents_collection.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 11:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OssException extends RuntimeException {
    private Integer code;

    private String msg;
}
