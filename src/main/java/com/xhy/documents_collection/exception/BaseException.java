package com.xhy.documents_collection.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 21:23
 * 权限校验异常类
 * 使用时请用异常捕获类捕获
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseException extends RuntimeException{

    private Integer code;

    private String msg;
}
