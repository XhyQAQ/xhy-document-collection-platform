package com.xhy.documents_collection.annotations;

import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author: Xhy
 * CreateTime: 2022-06-05 00:37
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FileLog {

    /**
     * 模块
     */
    public ServiceType title();

    /**
     * 功能
     */
    public BusinessType businessType() default BusinessType.OTHER;
}
