package com.xhy.documents_collection.authority;

import com.xhy.documents_collection.utils.JwtUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Author: Xhy
 * CreateTime: 2022-06-13 12:57
 */
@Component(value = "postMappingAuthorityVerify")
public class UnifyAuthorityVerify extends DefaultAuthorityVerify{
    @Override
    public Boolean authorityVerify(HttpServletRequest request,String... permissions) {

        String uId = JwtUtils.getMemberIdByJwtToken(request);
        for (String permission : permissions) {
            if (!AuthorityUtils.verify(uId,permission)) {
                return false;
            }
        }
        return true;
    }
}
