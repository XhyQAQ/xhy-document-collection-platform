package com.xhy.documents_collection.authority;

import com.xhy.documents_collection.exception.BaseException;
import com.xhy.documents_collection.utils.JwtUtils;


import javax.servlet.http.HttpServletRequest;

/**
 * Author: Xhy
 * CreateTime: 2022-11-14 17:22
 */
public class BaseAuthority implements AuthorityVerify {
    @Override
    public Boolean authorityVerify(HttpServletRequest request, String[] permissions) {
        if (!JwtUtils.checkToken(request)) {
            return false;
        }
        // 获取当前用户权限
        String uId = JwtUtils.getMemberIdByJwtToken(request);
        for (String permission : permissions) {
            if (!AuthorityUtils.verify(uId,permission)) {
                return false;
            }
        }

        return true;
    }
}
