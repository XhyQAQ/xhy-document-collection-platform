package com.xhy.documents_collection.restrict;

import com.xhy.documents_collection.restrict.impl.CounterRestrict;
import org.springframework.stereotype.Component;

/**
 * Author: Xhy
 * CreateTime: 2023-03-08 09:37
 * 限流处理器
 */
@Component
public class RestrictHandler implements Restrict{

    static Restrict restrict = new CounterRestrict();

    @Override
    public void set(String key, Integer value, long time) {
        restrict.set(key,value,time);
    }

    @Override
    public Integer get(String key) {
        return restrict.get(key);
    }

    @Override
    public void remove(String key) {
        restrict.remove(key);
    }

    @Override
    public void incr(String key,long time) {
        restrict.incr(key,time);
    }

    @Override
    public boolean check(String key, Integer limit) {
        return restrict.check(key,limit);
    }

    public static void setRestrict(Restrict r){
        restrict = r;
    }

    public static Restrict getRestrict(){
        return restrict;
    }
}
