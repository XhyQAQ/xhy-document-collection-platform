package com.xhy.documents_collection.thread_pool;

import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.scheduler.SchedulerHandler;
import com.xhy.documents_collection.service.task.TaskService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Author: Xhy
 * CreateTime: 2023-03-03 23:24
 * 拒绝策略,定时任务被拒绝后在此重试
 */
@Component
public class MyRejectExecutionHandler implements RejectedExecutionHandler {

    @Resource
    SchedulerHandler schedulerHandler;

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        if (!(r instanceof  MyRunnable)) return;
        MyRunnable myRunnable = (MyRunnable) r;
        // 重新加入延迟队列
        TaskTimingRecord taskTimingRecord = myRunnable.getTaskTimingRecord();
        if (taskTimingRecord !=null){
            // 往后延迟10秒
            Date sendTime = new Date(taskTimingRecord.getSendTime().getTime() + 10000);
            taskTimingRecord.setSendTime(sendTime);
            taskTimingRecord.setExpire();
            schedulerHandler.add(taskTimingRecord);
        }

    }
}
