package com.xhy.documents_collection.service.task.impl;

import com.xhy.documents_collection.entity.PO.task.TroopUser;
import com.xhy.documents_collection.mapper.task.TroopUserMapper;
import com.xhy.documents_collection.service.task.TroopUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2022-11-22
 */
@Service
public class TroopUserServiceImpl extends ServiceImpl<TroopUserMapper, TroopUser> implements TroopUserService {

}
