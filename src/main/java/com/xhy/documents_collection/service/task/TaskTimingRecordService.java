package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.utils.R;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-03-03
 */

public interface TaskTimingRecordService extends IService<TaskTimingRecord> {


    /**
     * 添加定时任务,一个任务一个定时,定时发送为任务的截止期
     * @param taskTimingRecord
     * @return
     */
    R addTask(TaskTimingRecord taskTimingRecord);

    /**
     * 修改定时任务
     * @param taskTimingRecords
     * @return
     */
    R updateTasks(Collection<TaskTimingRecord> taskTimingRecords);

    /**
     * 删除定时任务
     * @param asList
     * @return
     */
    R removeTaskByIds(List<Integer> asList);

    /**
     * 手动发送定时任务
     * @param id
     */
    void artificialSend(Integer id);
}
