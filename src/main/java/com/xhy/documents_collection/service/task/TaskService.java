package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.Task;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.utils.R;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TaskService extends IService<Task> {

    /**
     * 发布任务
     * @param task
     * @return
     */
    R issueTask(Task task);

    /**
     * 删除任务以及任务下的所有作业记录(包括文件)
     * @param ids
     * @return
     */
    R deleteTasks(List<Integer> ids);

    Integer getTeamIdByTaskId(Integer taskId);

    String getTaskName(Integer taskId);

    /**
     * 获取任务
     * @param taskId 任务id
     * @return
     */
    Task get(Integer taskId);

    /**
     * 修改任务
     * @param task 任务
     * @return
     */
    R update(Task task);

    /**
     * 获取学生未完成的作业(在首页快捷显示)
     * @return
     */
    R noSubmit();
}
