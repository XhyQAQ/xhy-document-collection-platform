package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.Group;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.utils.R;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-22
 */
public interface GroupService extends IService<Group> {

    /**
     * 邀请
     * @param taskId 任务id
     * @param inviteId 邀请者id
     * @return
     */
    R invite(Integer taskId, Integer inviteId);

    /**
     * 取消邀请
     * @param taskId 任务id
     * @param inviteId 邀请人的id
     * @return
     */
    R cancelInvite(Integer taskId, Integer inviteId);

    R exitGroup(Integer taskRecordId);
}
