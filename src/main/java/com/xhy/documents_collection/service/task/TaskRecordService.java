package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.TaskRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TaskRecordService extends IService<TaskRecord> {

    /**
     * 获取作业记录
     * @param taskRecordId 任务记录id
     * @return
     */
    TaskRecord get(String taskRecordId);

    Integer findUid(Integer taskId, Integer inviteId);

    /**
     * 组队的功能，组队已废弃
     * @param taskId
     * @param uId
     * @return
     */
    @Deprecated
    Integer findTaskRecordId(Integer taskId,Integer uId);
}
