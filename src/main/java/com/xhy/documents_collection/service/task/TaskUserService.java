package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.TaskUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-22
 */
public interface TaskUserService extends IService<TaskUser> {

}
