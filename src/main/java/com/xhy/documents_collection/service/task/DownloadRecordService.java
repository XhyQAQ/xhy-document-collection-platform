package com.xhy.documents_collection.service.task;

import com.xhy.documents_collection.entity.PO.task.DownloadRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.utils.R;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-02-07
 */
public interface DownloadRecordService extends IService<DownloadRecord> {

    R removeByIdsAndUserId(List<Integer> idList);

}
