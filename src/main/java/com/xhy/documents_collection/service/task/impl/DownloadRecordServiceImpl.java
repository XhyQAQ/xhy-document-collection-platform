package com.xhy.documents_collection.service.task.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.entity.PO.task.DownloadRecord;
import com.xhy.documents_collection.mapper.task.DownloadRecordMapper;
import com.xhy.documents_collection.service.task.DownloadRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhy.documents_collection.utils.R;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2023-02-07
 */
@Service
public class  DownloadRecordServiceImpl extends ServiceImpl<DownloadRecordMapper, DownloadRecord> implements DownloadRecordService {


    @Resource
    DownloadRecordMapper downloadRecordMapper;

    @Override
    public R removeByIdsAndUserId(List<Integer> idList) {

        if (ObjectUtils.isEmpty(idList)){
            return R.error().message("请选择记录删除");
        }
        Integer userId = UserHolder.get();
        remove(new LambdaQueryWrapper<DownloadRecord>()
        .in(DownloadRecord::getId,idList).eq(DownloadRecord::getUserId,userId));

        return R.ok().message("删除成功");
    }
}
