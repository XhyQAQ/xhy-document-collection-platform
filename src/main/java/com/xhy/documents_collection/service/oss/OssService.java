package com.xhy.documents_collection.service.oss;

import com.xhy.documents_collection.utils.R;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * Author: Xhy
 * CreateTime: 2022-11-19 21:20
 */
public interface OssService {

    /**
     * 团队负责人上传文件
     * @param file 文件
     * @param taskRecordId 文件记录id
     * @return
     */
    R captionUpload(MultipartFile file, String taskRecordId);

    /**
     * 团队负责人退回文件
     * @param taskRecordId 具体的作业记录id
     * @return
     */
    R captionSendBack(String taskRecordId);

    /**
     * 负责人下载文件,会上传到OSS后记录url供再次下载
     * 这样做是为了在下很多文件的时候会卡死现象，因此分为两步下载
     * @param taskRecordIds 作业记录ids
     * @param response
     * @return
     */
    R download(String taskRecordIds, HttpServletResponse response);

    /**
     * 用户上传
     * @param file 文件
     * @param taskRecordId 具体的作业记录id
     * @return
     */
    R userUpload(MultipartFile file, String taskRecordId);

    /**
     * 用户退回文件
     * @param taskRecordId 具体的作业记录id
     * @return
     */
    R userSendBack(String taskRecordId);

    /**
     * 下载文件
     * 该接口对团队负责人和成员下载文件通用
     * @param id 团队负责人传下载记录id,成员传文件记录id
     * return 签名URL
     */
    String downloadFile(Integer id);
}
