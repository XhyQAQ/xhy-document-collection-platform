package com.xhy.documents_collection.service.async;

import com.xhy.documents_collection.entity.DO.UploadDO;
import com.xhy.documents_collection.entity.PO.email.EmailSetting;
import com.xhy.documents_collection.entity.PO.task.TaskRecord;
import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;

import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2022-12-15 16:13
 */
public interface AsyncService {

    /**
     * 上传文件
     * @param uploadDO
     */
    void upload(UploadDO uploadDO);

    /**
     * 删除文件
     * @param taskRecord 文件记录
     * @param uId 用户id
     */
    void deleteFile(TaskRecord taskRecord,Integer uId);

    /**
     * 删除任务以及任务下的文件
     * @param taskIds 任务id
     */
    void removeByTaskIds(List<Integer> taskIds);

    /**
     * 下载文件
     * @param records 任务记录ids
     * @param userId 用户id
     */
    void downloadFiles(List<TaskRecord> records,Integer userId);

    /**
     * 上传作业描述文件
     * @param uploadDO
     */
    void uploadTaskDescription(UploadDO uploadDO);

    /**
     * 删除文件
     * @param filePaths 文件路径
     */
    void deleteFiles(List<String> filePaths);

    /**
     * 测试邮箱是否配置成功
     * @param emailSetting 邮箱配置
     */
    void sendTestEmail(EmailSetting emailSetting);

    /**
     * 添加定时任务
     * @param taskTimingRecord
     */
    void addSchedulerTask(TaskTimingRecord taskTimingRecord);


}
