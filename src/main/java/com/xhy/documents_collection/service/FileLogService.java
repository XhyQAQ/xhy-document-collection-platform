package com.xhy.documents_collection.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.entity.PO.log.FileLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.Collections;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-03-15
 */
public interface FileLogService extends IService<FileLog> {

    /**
     * 查出文件日志
     * @param current
     * @param limit
     * @param ids
     * @return
     */
    Page<FileLog> page(Long current, Long limit, Collection ids);

    /**
     * 修改日志状态
     * @param taskRecordId
     * @param uId
     */
    void updateState(Integer taskRecordId, Integer uId);
}
