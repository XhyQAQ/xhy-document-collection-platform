package com.xhy.documents_collection.service.impl;

import com.xhy.documents_collection.entity.PO.email.EmailTable;
import com.xhy.documents_collection.mapper.EmailTableMapper;
import com.xhy.documents_collection.service.EmailTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2023-03-14
 */
@Service
public class EmailTableServiceImpl extends ServiceImpl<EmailTableMapper, EmailTable> implements EmailTableService {

}
