package com.xhy.documents_collection.service.impl;

import com.xhy.documents_collection.entity.PO.log.LoginLog;
import com.xhy.documents_collection.mapper.LoginLogMapper;
import com.xhy.documents_collection.service.LoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2023-03-19
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements LoginLogService {

}
