package com.xhy.documents_collection.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhy.documents_collection.cache.UserCache;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.xhy.documents_collection.entity.PO.team.TeamUser;
import com.xhy.documents_collection.entity.PO.User;
import com.xhy.documents_collection.entity.PO.team.UserRole;
import com.xhy.documents_collection.entity.excel.UserExcel;
import com.xhy.documents_collection.service.*;

import com.xhy.documents_collection.service.authority.UserRoleService;
import com.xhy.documents_collection.service.team.TeamService;
import com.xhy.documents_collection.service.team.TeamUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2022-07-10 19:44
 */
@Service
public class ExcelServiceImpl implements ExcelService {

    @Resource
    private UserService userService;

    @Resource
    private TeamUserService teamUserService;


    @Resource
    private UserRoleService userRoleService;

    @Resource
    private TeamService teamService;

    @Override
    public void read(InputStream file,Integer id) {

        EasyExcel.read(file, UserExcel.class, new UploadDataListener(id)).sheet().doRead();
    }

    private class UploadDataListener implements ReadListener<UserExcel> {

        private Integer tId;
        public UploadDataListener(Integer id){
            tId = id;
        }
        public UploadDataListener(){

        }
        private List<User> data = new ArrayList<>();
        // 入口 一行一行读
        @Override
        public void invoke(UserExcel userExcel, AnalysisContext analysisContext) {
            User user = new User();
            user.setEmail(userExcel.getEmail());
            user.setName(userExcel.getUserName());
            // 邮箱存在则用邮箱作为密码，不存在则用学号作为密码
            user.setPassword(ObjectUtils.isEmpty(userExcel.getEmail()) ? userExcel.getStudentNumber(): userExcel.getEmail());
            user.setStudentNumber(userExcel.getStudentNumber());
            data.add(user);
        }

        // 全部读完后执行的方法
        @Override
        public void doAfterAllAnalysed(AnalysisContext analysisContext) {
           saveData();
           data.clear();
        }

        // 数据库中不存在用户则创建,存在则添加团队角色表
        private void saveData(){

            // 1.查询数据库当前用户是否存在，不存在则创建
            for (User user : data) {
                // 查询邮箱或者学号是否存在
                User userInfo = userService.getOne(new QueryWrapper<User>().eq("email", user.getEmail()).or().eq("student_number", user.getStudentNumber()));
                if (userInfo == null){
                    userService.save(user);
                }else {
                    user.setId(userInfo.getId());
                }
            }
            ArrayList<TeamUser> teamUsers = new ArrayList<>();
            ArrayList<UserRole> userRoles = new ArrayList<>();
            // 添加关系:团队成员关系，普通用户权限
            for (User user : data) {
                TeamUser teamUser = new TeamUser();
                Integer uId = user.getId();
                teamUser.setUId(uId);
                teamUser.setTId(tId);
                teamUsers.add(teamUser);
                UserRole userRole = new UserRole();
                userRole.setUId(uId);
                userRole.setRId(14);
                userRoles.add(userRole);
            }

            userRoleService.saveBatch(userRoles);
            teamUserService.saveBatch(teamUsers);
            UserCache.refresh();
        }
    }
}
