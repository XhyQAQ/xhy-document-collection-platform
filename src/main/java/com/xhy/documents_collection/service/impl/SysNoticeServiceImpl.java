package com.xhy.documents_collection.service.impl;

import com.xhy.documents_collection.entity.PO.Notice;
import com.xhy.documents_collection.mapper.SysNoticeMapper;
import com.xhy.documents_collection.service.SysNoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2023-03-20
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, Notice> implements SysNoticeService {

}
