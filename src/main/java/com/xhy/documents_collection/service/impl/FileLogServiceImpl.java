package com.xhy.documents_collection.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.cache.UserCache;
import com.xhy.documents_collection.entity.PO.log.FileLog;
import com.xhy.documents_collection.entity.PO.task.Task;
import com.xhy.documents_collection.entity.PO.task.TaskRecord;
import com.xhy.documents_collection.entity.PO.team.Team;
import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.mapper.FileLogMapper;
import com.xhy.documents_collection.service.FileLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhy.documents_collection.service.UserService;
import com.xhy.documents_collection.service.task.TaskRecordService;
import com.xhy.documents_collection.service.task.TaskService;
import com.xhy.documents_collection.service.team.TeamService;
import com.xhy.documents_collection.utils.R;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2023-03-15
 */
@Service
public class FileLogServiceImpl extends ServiceImpl<FileLogMapper, FileLog> implements FileLogService {

    @Resource
    TaskRecordService taskRecordService;

    @Resource
    UserService userService;

    @Resource
    private TeamService teamService;

    @Resource
    private TaskService taskService;

    @Override
    public Page<FileLog> page(Long current, Long limit, Collection ids) {

        Page<FileLog> page = page(new Page<>(current, limit), new LambdaQueryWrapper<FileLog>()
                .in(!ObjectUtils.isEmpty(ids), FileLog::getUId, ids)
                .orderByDesc(FileLog::getGmtCreated));
        if (page.getTotal() == 0L){
            return page;
        }
        List<Integer> taskRecordIds = page.getRecords().stream().map(FileLog::getTaskRecordId).collect(Collectors.toList());
        // taskRecordId -> taskId
        Map<Integer, Integer> taskRecordIdAndTaskIdMap = taskRecordService.list(new LambdaQueryWrapper<TaskRecord>()
                .select(TaskRecord::getId, TaskRecord::getTId).in(TaskRecord::getId,taskRecordIds)).stream().collect(Collectors.toMap(TaskRecord::getId, TaskRecord::getTId));
        Collection<Integer> taskIds = taskRecordIdAndTaskIdMap.values();

        List<Task> tasks = taskService.list(new LambdaQueryWrapper<Task>().select(Task::getId, Task::getTId, Task::getName).in(Task::getId, taskIds));
        // taskId -> teamId
        Map<Integer, Integer> taskIdAndTeamIdMap = tasks.stream().collect(Collectors.toMap(Task::getId, Task::getTId));
        List<Integer> teamIds = tasks.stream().map(Task::getTId).collect(Collectors.toList());
        // taskId -> taskName
        Map<Integer, String> taskNameMap = tasks.stream().collect(Collectors.toMap(Task::getId, Task::getName));
        // teamId -> teamName
        Map<Integer, String> teamNameMap = teamService.list(new LambdaQueryWrapper<Team>().select(Team::getId, Team::getName).in(Team::getId,teamIds))
                .stream().collect(Collectors.toMap(Team::getId, Team::getName));
        for (FileLog fileLog : page.getRecords()) {
            fileLog.setBusinessTypeName(BusinessType.getName(fileLog.getBusinessType()));
            Integer taskId = taskRecordIdAndTaskIdMap.get(fileLog.getTaskRecordId());
            fileLog.setTaskName(taskNameMap.get(taskId));
            Integer teamId = taskIdAndTeamIdMap.get(taskId);
            fileLog.setTeamName(teamNameMap.get(teamId));
            fileLog.setUserName(UserCache.get(fileLog.getUId()));
        }
        return page;
    }

    @Override
    public void updateState(Integer taskRecordId, Integer uId) {
        FileLog fileLog = getOne(new LambdaQueryWrapper<FileLog>()
                .eq(FileLog::getTaskRecordId, taskRecordId).eq(FileLog::getUId, uId));
        if (!ObjectUtils.isEmpty(fileLog)){
            fileLog.setState(false);
            updateById(fileLog);
        }
    }
}
