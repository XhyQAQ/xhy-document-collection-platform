package com.xhy.documents_collection.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xhy.documents_collection.enums.TimingEnum;
import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.service.DelayService;
import com.xhy.documents_collection.service.task.TaskTimingRecordService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

/**
 * Author: Xhy
 * CreateTime: 2023-03-03 18:52
 */
@Deprecated
public class DelayServiceImpl implements DelayService{


    private BlockingQueue<TaskTimingRecord> queue;

    @Resource
    TaskTimingRecordService taskTimingRecordService;





    @Override
    public void add(TaskTimingRecord taskTimingRecord) {
        queue.add(taskTimingRecord);

    }

    @Override
    public void update(TaskTimingRecord taskTimingRecord) {
        if (!queue.isEmpty()){
            TaskTimingRecord t = taskTimingRecordService.getById(taskTimingRecord.getId());
            taskTimingRecord.setTaskName(t.getTaskName());
            taskTimingRecord.setUserId(UserHolder.get());
            queue.remove(taskTimingRecord);
            queue.add(taskTimingRecord);
        }
    }

    @Override
    public void remove(List<Integer> ids) {
        for (Integer id : ids) {
            TaskTimingRecord taskTimingRecord = new TaskTimingRecord(id);
            queue.remove(taskTimingRecord);
        }
    }

}
