package com.xhy.documents_collection.service;

import com.xhy.documents_collection.entity.PO.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-03-20
 */
public interface SysNoticeService extends IService<Notice> {

}
