package com.xhy.documents_collection.service;

import com.xhy.documents_collection.entity.PO.log.LoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-03-19
 */
public interface LoginLogService extends IService<LoginLog> {

}
