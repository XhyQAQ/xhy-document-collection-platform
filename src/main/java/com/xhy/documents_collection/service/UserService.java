package com.xhy.documents_collection.service;

import com.xhy.documents_collection.entity.PO.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.utils.R;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface UserService extends IService<User> {

    /**
     * 根据邮箱获取用户
     * @param email
     * @return
     */
    User getUserByEmail(String email);

    /**
     * 根据学号获取用户
     * @param studentNumber
     * @return
     */
    User getUserByStudentNumber(String studentNumber);

    /**
     * 添加用户
     * @param user
     * @return
     */
    R addUser(User user);

    /**
     * 删除用户
     * @param ids
     * @return
     */
    R removeUsers(String ids);

    /**
     * 修改用户
     * @param user
     * @return
     */
    R updateUser(User user);
}
