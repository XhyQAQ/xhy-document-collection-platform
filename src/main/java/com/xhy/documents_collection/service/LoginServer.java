package com.xhy.documents_collection.service;

import com.xhy.documents_collection.utils.R;

/**
 * Author: Xhy
 * CreateTime: 2022-11-14 16:59
 */
public interface LoginServer {
    /**
     * 登录
     * @param username 学号/邮箱
     * @param password
     * @return
     */
    R login(String username, String password);
}
