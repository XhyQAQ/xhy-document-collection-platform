package com.xhy.documents_collection.service.authority.impl;

import com.xhy.documents_collection.entity.PO.authority.RolePermission;
import com.xhy.documents_collection.mapper.authority.RolePermissionMapper;
import com.xhy.documents_collection.service.authority.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
