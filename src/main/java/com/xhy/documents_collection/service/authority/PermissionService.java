package com.xhy.documents_collection.service.authority;

import com.xhy.documents_collection.entity.PO.authority.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface PermissionService extends IService<Permission> {

    Map<String, Object> toTree(HttpServletRequest request);


    List<Permission> treeSelect();


    void removeMenu(Integer id);

}
