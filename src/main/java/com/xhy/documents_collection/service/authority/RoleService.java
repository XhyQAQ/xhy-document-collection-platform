package com.xhy.documents_collection.service.authority;

import com.xhy.documents_collection.entity.PO.authority.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.entity.PO.authority.Tree;
import com.xhy.documents_collection.entity.VO.AssignRoleVO;
import com.xhy.documents_collection.entity.VO.AuthorityVO;
import com.xhy.documents_collection.utils.R;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface RoleService extends IService<Role> {
    List<Tree> tree();

    R removeRole(String id);

    R gavePermission(AuthorityVO authorityVO);

    R gaveRole(AssignRoleVO assignRoleVO);
}
