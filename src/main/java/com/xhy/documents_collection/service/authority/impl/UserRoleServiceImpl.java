package com.xhy.documents_collection.service.authority.impl;

import com.xhy.documents_collection.entity.PO.team.UserRole;
import com.xhy.documents_collection.mapper.authority.UserRoleMapper;
import com.xhy.documents_collection.service.authority.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
