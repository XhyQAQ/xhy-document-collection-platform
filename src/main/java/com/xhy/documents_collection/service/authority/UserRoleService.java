package com.xhy.documents_collection.service.authority;

import com.xhy.documents_collection.entity.PO.team.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface UserRoleService extends IService<UserRole> {

}
