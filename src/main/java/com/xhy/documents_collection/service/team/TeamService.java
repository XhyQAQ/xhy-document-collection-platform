package com.xhy.documents_collection.service.team;

import com.xhy.documents_collection.entity.PO.team.Team;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.entity.PO.User;
import com.xhy.documents_collection.utils.R;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TeamService extends IService<Team> {


    /**
     * 删除团队以及下的所有任务及所有文件记录(包括文件)
     * @param ids
     * @return
     */
    R removeTeams(String ids);

    /**
     * 添加团队
     * @param team
     * @return
     */
    R addTeam(Team team);

    /**
     * 修改团队
     * @param team
     * @return
     */
    R updateTeam(Team team);

    /**
     * 删除团队 为什么和上面是同样的功能 removeTeams() 我也不记得了，忘记怎么写的了
     * @param ids
     * @return
     */
    R deleteTeam(String ids);

    /**
     * 为团队中添加成员,成员不存在则创建
     * @param user  成员
     * @param teamId 团队id
     * @return
     */
    R addUser(User user,Integer teamId);

    /**
     * 提出团队中的成员
     * @param ids 成员ids
     * @param teamId 团队id
     * @return
     */
    R deleteUsers(String ids,Integer teamId);
}
