package com.xhy.documents_collection.service.team.impl;

import com.xhy.documents_collection.entity.PO.team.TeamUser;
import com.xhy.documents_collection.mapper.team.TeamUserMapper;
import com.xhy.documents_collection.service.team.TeamUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
@Service
public class TeamUserServiceImpl extends ServiceImpl<TeamUserMapper, TeamUser> implements TeamUserService {

}
