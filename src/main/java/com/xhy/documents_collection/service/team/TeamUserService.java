package com.xhy.documents_collection.service.team;

import com.xhy.documents_collection.entity.PO.team.TeamUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface TeamUserService extends IService<TeamUser> {

}
