package com.xhy.documents_collection.service;

import com.xhy.documents_collection.entity.PO.email.EmailSetting;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;
import com.xhy.documents_collection.utils.R;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2023-03-04
 */
public interface EmailSettingService extends IService<EmailSetting> {

    /**
     * 根据用户id获取邮箱配置,不存在则创建
     * @return
     */
    R getByUserId();

    /**
     * 修改邮箱配置
     * @param emailSetting
     * @return
     */
    R update(EmailSetting emailSetting);

    /**
     * 校验邮箱是否配置成功
     * @return
     */
    R testSend();

    /**
     * 创建JavaMailSenderImpl
     * @param emailSetting
     * @return
     */
    JavaMailSenderImpl createMessage(EmailSetting emailSetting);

    /**
     * 发送任务下的文件
     * @param taskTimingRecord
     * @return
     */
    boolean sendTaskFile(TaskTimingRecord taskTimingRecord);

    /**
     * 发送邮箱_测试
     * @param from
     * @param to
     * @param userId
     */
    void testSend(String from,String to,Integer userId);

    /**
     * 发送内容
     * @param emailSetting
     * @param content
     */
    void send(EmailSetting emailSetting, String content);
}
