package com.xhy.documents_collection.service;

import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;

import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2023-03-03 18:52
 */

@Deprecated
public interface DelayService {

    void add(TaskTimingRecord taskTimingRecord);

    void update(TaskTimingRecord taskTimingRecord);

    void remove(List<Integer> ids);


}
