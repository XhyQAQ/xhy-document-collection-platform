package com.xhy.documents_collection.service.sys;

import com.xhy.documents_collection.entity.PO.Setting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface SysSettingService extends IService<Setting> {

}
