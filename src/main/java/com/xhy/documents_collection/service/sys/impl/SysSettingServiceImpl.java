package com.xhy.documents_collection.service.sys.impl;

import com.xhy.documents_collection.entity.PO.Setting;
import com.xhy.documents_collection.mapper.sys.SysSettingMapper;
import com.xhy.documents_collection.service.sys.SysSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
@Service
public class SysSettingServiceImpl extends ServiceImpl<SysSettingMapper, Setting> implements SysSettingService {

}
