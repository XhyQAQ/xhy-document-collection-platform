package com.xhy.documents_collection.service.sys;

import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xhy
 * @since 2022-11-17
 */
public interface SysLogService extends IService<SysLog> {

}
