package com.xhy.documents_collection.service;

import java.io.InputStream;

/**
 * Author: Xhy
 * CreateTime: 2022-07-10 19:43
 */
public interface ExcelService {

    void read(InputStream file, Integer id);
}
