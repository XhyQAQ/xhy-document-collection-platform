package com.xhy.documents_collection.scheduler;

import java.util.Collection;
import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 10:24
 * 定时器处理器
 */
public class SchedulerHandler implements Scheduler{

    private Scheduler scheduler;


    public void setScheduler(Scheduler scheduler){
        this.scheduler = scheduler;
    }

    @Override
    public void add(SchedulerData schedulerData) {
        scheduler.add(schedulerData);
    }

    @Override
    public void update(SchedulerData schedulerData) {
        scheduler.update(schedulerData);
    }


    @Override
    public void delete(Collection ids) {
        scheduler.delete(ids);
    }


}
