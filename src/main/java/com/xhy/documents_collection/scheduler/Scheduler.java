package com.xhy.documents_collection.scheduler;

import java.util.Collection;
import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 10:21
 * 定时器接口
 */
public interface Scheduler <T extends SchedulerData>{

    void add(T t);

    void update(T t);

    void delete(Collection<Integer> ids);


}
