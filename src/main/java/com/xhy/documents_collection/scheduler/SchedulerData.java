package com.xhy.documents_collection.scheduler;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 10:22
 * 定时器数据对象,如果需要定制其他定时器携带对象，对象需要实现该接口
 */
public interface SchedulerData {
}
