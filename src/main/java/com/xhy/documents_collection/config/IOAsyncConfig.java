package com.xhy.documents_collection.config;

import com.xhy.documents_collection.thread_pool.MyRejectExecutionHandler;
import com.xhy.documents_collection.utils.SpringUtil;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @description:
 * @Author: Xhy
 * @gitee: https://gitee.com/XhyQAQ
 * @copyright: B站: https://space.bilibili.com/152686439?spm_id_from=333.1007.0.0
 * @CreateTime: 2023-04-13 21:42
 */
@Configuration
public class IOAsyncConfig implements AsyncConfigurer {
    private static int cpu = Runtime.getRuntime().availableProcessors();



    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 配置核心线程数
        executor.setCorePoolSize(cpu);
        // 配置最大线程数
        executor.setMaxPoolSize(cpu*2);
        // 配置队列大小
        executor.setQueueCapacity(0);
        // 配置线程池中的线程的名词前缀
        executor.setThreadNamePrefix("io-");
        // 拒绝策略
        executor.setRejectedExecutionHandler(new MyRejectExecutionHandler());
        executor.initialize();
        return executor;
    }


}
