package com.xhy.documents_collection.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

@Component
public class MyDateConfig implements MetaObjectHandler {
  
  	// 创建时间的方法
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("gmtCreated",new Date(),metaObject);
        this.setFieldValByName("gmtModify",new Date(),metaObject);

    }
	
  
  // 更新时间的方法
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("gmtModify",new Date(),metaObject);
    }
}