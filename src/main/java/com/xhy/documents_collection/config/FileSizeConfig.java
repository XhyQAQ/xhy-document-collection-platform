package com.xhy.documents_collection.config;

import com.xhy.documents_collection.entity.PO.Setting;
import com.xhy.documents_collection.service.sys.SysSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

/**
 * Author: Xhy
 * CreateTime: 2022-07-05 23:14
 */
@Component
public class FileSizeConfig {

    @Autowired
    private SysSettingService settingService;

    @Bean
    public MultipartConfigElement multipartConfigElement(){
        MultipartConfigFactory factory = new MultipartConfigFactory();
        Setting setting = settingService.getById(1);
        // 单个文件
        factory.setMaxRequestSize(DataSize.ofMegabytes(setting.getSystemFileSize()));
        // 最大文件
        factory.setMaxFileSize(DataSize.ofMegabytes(setting.getSystemFileSize()));
        return factory.createMultipartConfig();
    }
}
