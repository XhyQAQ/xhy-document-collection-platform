package com.xhy.documents_collection.config;

import com.xhy.documents_collection.scheduler.DelayQueueScheduler;
import com.xhy.documents_collection.scheduler.SchedulerHandler;
import com.xhy.documents_collection.service.task.TaskTimingRecordService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 10:34
 */
@Configuration
public class SchedulerConfig {


    @Resource
    TaskTimingRecordService taskTimingRecordService;

    @Bean
    public SchedulerHandler schedulerHandler(){
        SchedulerHandler schedulerHandler = new SchedulerHandler();
        schedulerHandler.setScheduler(DelayQueueScheduler.getInstance(taskTimingRecordService));
        return schedulerHandler;
    }
}
