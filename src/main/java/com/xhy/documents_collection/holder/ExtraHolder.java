package com.xhy.documents_collection.holder;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 11:38
 */
public class ExtraHolder {

    private static ThreadLocal<String> extraThreadLocal = new ThreadLocal<>();

    // 添加
    public static void set(String id){
        extraThreadLocal.set(id);
    }
    // 获取
    public static String get(){
        return extraThreadLocal.get();
    }

    // 删除
    public static void clear(){
        extraThreadLocal.remove();
    }
}
