package com.xhy.documents_collection.holder;

/**
 * Author: Xhy
 * CreateTime: 2022-11-18 08:27
 * 用户ThreadLocal
 */
public class UserHolder {
    private static ThreadLocal<Integer> userThreadLocal = new ThreadLocal<>();

    // 添加
    public static void set(Object id){
        userThreadLocal.set(Integer.valueOf(id.toString()));
    }
    // 获取
    public static Integer get(){
        return userThreadLocal.get();
    }
    // 删除
    public static void clear(){
        userThreadLocal.remove();
    }
}
