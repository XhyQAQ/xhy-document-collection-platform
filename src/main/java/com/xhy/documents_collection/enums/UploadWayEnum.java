package com.xhy.documents_collection.enums;

/**
 * Author: Xhy
 * CreateTime: 2023-03-03 16:54
 * 文件下载方式
 */
public enum  UploadWayEnum {

    MO_UPLOAD(0,"手动下载"),
    TIMING_UPLOAD(1,"定时下载");

    public int type;
    public String info;
    UploadWayEnum(int type,String info){
        this.type = type;
        this.info = info;
    }

    public static String getInfo(int type){
        return values()[type].info;
    }
}
