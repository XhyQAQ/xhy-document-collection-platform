package com.xhy.documents_collection.enums;

/**
 * Author: Xhy
 * CreateTime: 2023-03-07 08:47
 * 邮箱
 */
public enum  EmailEnum {

    NOTICE(0,"通知"),// 用于告知任务状态
    SUBMIT(1,"提交");// 用于提交作业(带附件)

    public int type;
    public String info;
    EmailEnum(int type, String info){
        this.type = type;
        this.info = info;
    }

    public static String getInfo(int type){
        return values()[type].info;
    }
}
