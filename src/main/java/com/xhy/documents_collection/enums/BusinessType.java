package com.xhy.documents_collection.enums;


/**
 * Author: Xhy
 * CreateTime: 2023-03-15 13:55
 * 请求类型
 */
public enum  BusinessType {

    /**
     * 其它
     */
    OTHER("其他"),

    /**
     * 新增
     */
    INSERT("新增"),

    /**
     * 修改
     */
    UPDATE("修改"),

    /**
     * 删除
     */
    DELETE("删除"),

    /**
     * 授权
     */
    GRANT("授权"),

    /**
     * 上传  5
     */
    UPLOAD("上传"),

    /**
     * 下载 6
     */
    DOWNLOAD("下载"),

    /**
     * 文件退回 7
     */
    SEND_BACK("退回"),
    /**
     * 定时
     */
    SCHEDULER("定时任务"),
    /**
     * 确认
     */
    RECEIVE("确认")
    ;

    String name;
    BusinessType(String name){
        this.name = name;
    }

    public static String getName(Integer businessType) {
        return values()[businessType].name;
    }
}
