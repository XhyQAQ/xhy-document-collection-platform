package com.xhy.documents_collection.enums;

/**
 * Author: Xhy
 * CreateTime: 2023-03-03 16:54
 * 定时任务
 */
public enum TimingEnum {

    PROCESS(0,"进行中"),
    SUCCESS(1,"发送成功"),
    FAILED(2,"发送失败");

    public int type;
    public String info;
    TimingEnum(int type, String info){
        this.type = type;
        this.info = info;
    }

    public static String getInfo(int type){
        return values()[type].info;
    }
}
