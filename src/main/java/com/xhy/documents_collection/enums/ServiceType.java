package com.xhy.documents_collection.enums;

/**
 * Author: Xhy
 * CreateTime: 2023-03-15 15:20
 * 模块
 */
public enum ServiceType {
    USER("用户"),
    TEAM("团队"),
    TASK("任务"),
    TASK_RECORD("任务记录"),
    OSS("云存储"),
    LOGIN("登录"),
    PERMISSION("权限"),
    GROUP("组队"),
    CACHE("缓存"),
    THREAD_POOL("线程池"),
    EMAIL("邮箱"),
    EMAIL_SETTING("邮箱配置"),
    SCHEDULER("定时配置"),
    NOTICE("公告");

    final String name;
    ServiceType(String name){

        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public static String getName(int type){

        return values()[type].name();
    }

}
