package com.xhy.documents_collection;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.xhy.documents_collection.authority.AuthorityUtils;
import com.xhy.documents_collection.authority.BaseAuthority;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;

@SpringBootApplication
@MapperScan(basePackages = "com.xhy.documents_collection.mapper")
@EnableTransactionManagement
@EnableAsync
@PropertySource("classpath:/application.yml")
public class DocumentsCollectionApplication {


    public static void main(String[] args) {
        // 开启权限
        AuthorityUtils.setGlobalVerify(true,new BaseAuthority());
        SpringApplication.run(DocumentsCollectionApplication.class, args);

    }
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.H2));
        return interceptor;
    }

}
