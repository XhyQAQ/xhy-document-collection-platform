package com.xhy.documents_collection.entity.res;

import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2023-04-06 15:07
 */
@Data
public class OSSRes {
    private String path;
    private Boolean state;
    private String message;

    // 成功
    public static OSSRes ok(String path){

        return new OSSRes(path,true,"");
    }
    // 失败
    public static OSSRes failed(String message){
        return new OSSRes("",false,message);
    }

    public OSSRes() {
    }

    public OSSRes(String path, Boolean state, String message) {
        this.path = path;
        this.state = state;
        this.message = message;
    }
}
