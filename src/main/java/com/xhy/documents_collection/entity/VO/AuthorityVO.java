package com.xhy.documents_collection.entity.VO;

import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 19:26
 */
@Data
public class AuthorityVO {

    private Integer rid;

    private Integer[] pid;
}
