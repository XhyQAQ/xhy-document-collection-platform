package com.xhy.documents_collection.entity.VO;

import lombok.Data;

import java.util.Date;
import java.util.Objects;

/**
 * Author: Xhy
 * CreateTime: 2023-04-02 15:10
 */
@Data
public class DarkRoomUser {

    private Integer id;

    private String name;

    private String key;

    private Date banTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DarkRoomUser that = (DarkRoomUser) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
