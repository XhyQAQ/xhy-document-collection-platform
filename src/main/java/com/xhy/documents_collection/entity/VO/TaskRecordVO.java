package com.xhy.documents_collection.entity.VO;

import lombok.Data;

import java.util.Date;

/**
 * Author: Xhy
 * CreateTime: 2023-03-13 22:37
 */
@Data
public class TaskRecordVO {

    private Integer id;

    private String name;

    private String fileSizeString;

    private Boolean isSubmit;

    private String fileName;

    private Date submitTime;

    private String url;

    private String praFileName;

}
