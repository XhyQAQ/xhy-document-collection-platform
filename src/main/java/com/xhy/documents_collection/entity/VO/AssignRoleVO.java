package com.xhy.documents_collection.entity.VO;

import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 21:43
 */
@Data
public class AssignRoleVO {
    private Integer uId;
    private Integer[] rId;
}
