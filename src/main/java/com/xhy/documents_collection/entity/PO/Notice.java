package com.xhy.documents_collection.entity.PO;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2023-03-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_notice")
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "标题不可为空")
    private String title;

    @NotBlank(message = "内容不可为空")
    private String content;

    private Integer userId;

    private Boolean isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;


}
