package com.xhy.documents_collection.entity.PO;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "请输入昵称")
    private String name;

    private String password;

    private String studentNumber;

    @Email(message = "请输入正确的邮箱")
    private String email;

    private Boolean noticeState;

    @TableField(exist = false)
    private Boolean isOpenEmail;

    @TableLogic
    private Integer isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;


    @TableField(exist = false)
    private Set<String> roleName;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) ||
                Objects.equals(studentNumber, user.studentNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, studentNumber);
    }
}
