package com.xhy.documents_collection.entity.PO.task;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DownloadRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer taskId;

    @TableField(exist = false)
    private String taskName;

    private Integer userId;

    private String url;

    private Long fileSize;

    @TableField(exist = false)
    private String fileSizeString;

    private Integer size;

    private String filePath;

    private String fileName;

    private Integer uploadWay;

    private Integer downloadSize;

    @TableField(exist = false)
    private String uploadWayName;

    @TableLogic
    private Boolean isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;


}
