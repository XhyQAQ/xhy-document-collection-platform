package com.xhy.documents_collection.entity.PO.authority;

import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 15:05
 */
@Data
public class MenuKey {
    private String title;
    private String href;
    private String image;
}
