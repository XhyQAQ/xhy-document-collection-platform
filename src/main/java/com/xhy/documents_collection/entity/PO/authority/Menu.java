package com.xhy.documents_collection.entity.PO.authority;

import lombok.Data;

import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 15:07
 */
@Data
public class Menu {

    private Integer id;

    private Integer pId;

    private String path;

    private String href;

    private String icon;

    private String title;

    private String target;

    private Integer isMenu;
    private List<Menu> child;
}
