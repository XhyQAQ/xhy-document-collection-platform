package com.xhy.documents_collection.entity.PO.authority;

import lombok.Data;

import java.util.List;

/**
 * Author: Xhy
 * CreateTime: 2022-06-04 19:14
 */
@Data
public class Tree {

    private Integer id;

    private Integer pId;

    private String title;

    private List<Tree> children;

    private Boolean spread;
}
