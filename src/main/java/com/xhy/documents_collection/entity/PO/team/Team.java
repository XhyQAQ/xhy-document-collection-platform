package com.xhy.documents_collection.entity.PO.team;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "团队名称不可为空")
    private String name;

    private String description;

    private String invitationCode;

    private Integer uId;
    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private Integer count;

    private Long fileSizeCapacity;

    private Long fileSizeUsed;

    @TableField(exist = false)
    private String fileSizeCapacityShow;

    @TableLogic
    private Integer isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;

}
