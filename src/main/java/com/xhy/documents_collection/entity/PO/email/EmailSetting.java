package com.xhy.documents_collection.entity.PO.email;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2023-03-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EmailSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "host不可为空")
    private String host;

    @NotBlank(message = "账号不可为空")
    private String userName;

    @NotBlank(message = "密码不可为空")
    private String password;

    private Integer userId;

    // 邮箱配置状态:是否能使用
    private Boolean state;

    @TableLogic
    private Integer isDeleted;


}
