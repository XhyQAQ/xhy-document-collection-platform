package com.xhy.documents_collection.entity.PO.email;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2023-03-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EmailTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Email(message = "邮箱格式不正确")
    private String email;

    private String description;

    private Integer userId;

    private Integer isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;


}
