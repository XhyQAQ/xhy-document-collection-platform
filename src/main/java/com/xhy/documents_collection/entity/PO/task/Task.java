package com.xhy.documents_collection.entity.PO.task;

import com.baomidou.mybatisplus.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "任务名称不可为空")
    private String name;

    private String description;

    @NotNull(message = "请选择对应的团队")
    private Integer tId;

    @TableField(exist = false)
    private String teamName;

    private Integer uId;

    private String filePath;

    private String url;

    @TableField(exist = false)
    private MultipartFile file;

    // 幂等性,使用团队名称+任务名称+uId做唯一性
    private String taskNameUuid;

    // 文件名模板
    private String fileNameTemplate;

    @TableField(exist = false)
    private Boolean isTemplate;

    // 任务是否可分组 0为不分组,1为分组,默认为0
    @Range(min = 0,max = 1,message = "请输入正确的数据")
    private Integer isGroup;

    // 可组队后每个小组的人数限制
    private Integer groupUserLimit;

    @TableField(exist = false)
    private Boolean taskTimeState;

    @NotNull(message = "请选择开始时间")
    private Date startTime;

    @NotNull(message = "请选择结束时间")
    private Date endTime;

    @TableField(exist = false)
    private String finishState;

    @TableField(exist = false)
    private Boolean timeState;


    @TableLogic
    private Boolean isDeleted;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;

    public void setStartTime(String startTime) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startTime = simpleDateFormat.parse(startTime);
    }

    public void setEndTime(String endTime) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.endTime = simpleDateFormat.parse(endTime);
    }
}
