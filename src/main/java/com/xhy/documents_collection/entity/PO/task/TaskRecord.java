package com.xhy.documents_collection.entity.PO.task;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TaskRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer tId;

    @TableField(exist = false)
    private String userName;
    private Integer uId;

    @TableField(exist = false)
    private Boolean isSelf;

    @TableField(exist = false)
    private Boolean isGroup;

    @TableField(exist = false)
    private String groupName;

    @TableField(exist = false)
    private String praFileName;

    private String filePath;

    private String fileName;

    private Long fileSize;

    private String url;

    @TableField(exist = false)
    private String fileSizeString;

    private Boolean isSubmit;

    private Date submitTime;

    private String groupUserId;

    private Integer troopId;

    // 是否被其他玩家邀请
    @TableField(exist = false)
    private Boolean isMeBeInvited;

    // 其他玩家是否被自己邀请
    @TableField(exist = false)
    private Boolean isOtherUserBeInvited;

    @TableLogic
    private Boolean isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;


}
