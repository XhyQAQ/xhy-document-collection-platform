package com.xhy.documents_collection.entity.PO.log;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.util.Date;

/**
 * Author: Xhy
 * CreateTime: 2023-03-15 14:06
 */
@Data
public class Log {

    // 模块名
    private String title;

    private Integer businessType;

    private Integer level;

    private String content;

    private Integer uId;

    private String ip;

    private Boolean state;

    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    private String businessTypeName;

    @TableField(exist = false)
    private String levelName;

    @TableLogic
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModify;

}
