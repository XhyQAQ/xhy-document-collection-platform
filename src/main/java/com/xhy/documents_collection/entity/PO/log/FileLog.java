package com.xhy.documents_collection.entity.PO.log;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xhy
 * @since 2023-03-15
 * 用于记录文件上传下载日志
 * 已废弃
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FileLog extends Log implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String fileSize;

    private String fileName;

    private Integer taskRecordId;

    private String os;

    @TableField(exist = false)
    private String taskName;

    @TableField(exist = false)
    private String teamName;

}
