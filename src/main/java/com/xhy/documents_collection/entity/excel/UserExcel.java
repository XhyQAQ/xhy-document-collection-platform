package com.xhy.documents_collection.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2022-07-10 19:46
 */
@Data
public class UserExcel {
    @ExcelProperty(value = "邮箱")
    private String email;
    @ExcelProperty(value = "学号")
    private String studentNumber;
    @ExcelProperty(value = "姓名")
    private String userName;
}
