package com.xhy.documents_collection.entity.DO;

import lombok.Data;

import java.io.InputStream;

/**
 * Author: Xhy
 * CreateTime: 2022-12-15 17:52
 */
@Data
public class FileDO {
    String fileName;
    long fileSize;
    InputStream inputStream;
    String path;
}
