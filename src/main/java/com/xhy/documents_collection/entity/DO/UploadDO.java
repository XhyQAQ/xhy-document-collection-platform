package com.xhy.documents_collection.entity.DO;

import com.xhy.documents_collection.entity.PO.task.TaskRecord;
import com.xhy.documents_collection.entity.PO.team.Team;
import lombok.Data;

/**
 * Author: Xhy
 * CreateTime: 2022-12-15 17:10
 */
@Data
public class UploadDO {
    FileDO fileDO;
    TaskRecord taskRecord;
    Team team;
    Integer uId;
    Integer taskId;
}
