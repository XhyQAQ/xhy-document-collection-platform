package com.xhy.documents_collection.controller;

import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.xhy.documents_collection.holder.ExtraHolder;
import com.xhy.documents_collection.service.LoginServer;
import com.xhy.documents_collection.utils.IpUtils;
import com.xhy.documents_collection.utils.JwtUtils;
import com.xhy.documents_collection.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Author: Xhy
 * CreateTime: 2022-11-14 16:58
 */

@RestController
public class LoginController {

    @Autowired
    private LoginServer loginServer;

    /**
     * 登录,支持学号/邮箱登录
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/login")
    public R login(@NotNull(message = "账号不可为空") String username,
                   @NotNull(message = "密码不可为空") String password,
                   HttpServletRequest request){
        ExtraHolder.set(IpUtils.getIpAddr(request));
        return loginServer.login(username,password);
    }



}
