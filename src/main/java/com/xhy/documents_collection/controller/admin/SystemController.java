package com.xhy.documents_collection.controller.admin;

import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.entity.PO.Setting;
import com.xhy.documents_collection.service.sys.SysSettingService;
import com.xhy.documents_collection.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Author: Xhy
 * CreateTime: 2022-11-16 16:00
 * 系统设置
 */

@RequestMapping("/admin/system")
@RestController
@Controller
public class SystemController {


    @Resource
    private SysSettingService sysSettingService;

    @GetMapping
    public R get(){
        return R.ok().data("data",sysSettingService.getById(1));
    }


    @PostMapping
    @Authority("admin:system:save")
    public R save(@RequestBody Setting setting){

        sysSettingService.saveOrUpdate(setting);

        return R.ok();
    }


}
