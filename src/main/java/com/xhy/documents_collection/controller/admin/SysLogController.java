package com.xhy.documents_collection.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.cache.UserCache;
import com.xhy.documents_collection.entity.PO.log.LoginLog;
import com.xhy.documents_collection.entity.PO.log.FileLog;
import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.service.FileLogService;
import com.xhy.documents_collection.service.LoginLogService;
import com.xhy.documents_collection.service.sys.SysLogService;
import com.xhy.documents_collection.utils.R;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2022-11-17
 * 日志
 */
@RestController
@RequestMapping("/admin/log")
public class SysLogController {


    @Resource
    private SysLogService logService;

    @Resource
    private FileLogService fileLogService;

    @Resource
    private LoginLogService loginLogService;


    @GetMapping("/oper/page")
    @Authority("admin:oper:log:page")
    public R operPage(@RequestParam(value = "page",defaultValue = "1") Long current,
                  @RequestParam(value = "limit",defaultValue = "15") Long limit,
                  @RequestParam(required = false) Integer title
    ){

        LambdaQueryWrapper<SysLog> wrapper = new LambdaQueryWrapper<>();
        // 不然会空指针
        if (!ObjectUtils.isEmpty(title)){
            wrapper.eq(SysLog::getTitle,ServiceType.values()[title].getName());
        }
        wrapper.orderByDesc(SysLog::getGmtCreated);

        Page<SysLog> page = logService.page(new Page<>(current, limit), wrapper);
        // 查出用户表
        for (SysLog log : page.getRecords()) {
            log.setBusinessTypeName(BusinessType.getName(log.getBusinessType()));
            log.setUserName(UserCache.get(log.getUId()));
        }

        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }

    @DeleteMapping("/oper/{ids}")
    @Authority("admin:oper:log:delete")
    public R delete(@PathVariable String ids){
        logService.removeByIds(Arrays.asList(ids.split(",")));
        return R.ok();
    }



    @GetMapping("/getServiceNames")
    public R getServiceNames(){
        return R.ok().data("data",ServiceType.values());
    }





    // 文件日志
    @GetMapping("/file/page")
    @Authority("admin:file:log:page")
    public R filePage(@RequestParam(value = "page",defaultValue = "1") Long current,
                  @RequestParam(value = "limit",defaultValue = "15") Long limit
    ){

        Page<FileLog> page = fileLogService.page(current,limit,null);

        if (page.getTotal() == 0L){
            return R.ok().data("data", Collections.EMPTY_LIST);
        }


        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }

    @DeleteMapping("/file/{ids}")
    @Authority("admin:file:log:delete")
    public R fileLogDelete(@PathVariable String ids){
        List<Integer> idList = Arrays.asList(ids.split(",")).stream().map(Integer::parseInt).collect(Collectors.toList());
        if (!ObjectUtils.isEmpty(idList)){
            fileLogService.removeByIds(idList);
        }
        return R.ok().message("删除成功");
    }


    @GetMapping("/login/page")
    @Authority("admin:login:log:page")
    public R loginPage(@RequestParam(value = "page",defaultValue = "1") Long current,
                      @RequestParam(value = "limit",defaultValue = "15") Long limit
    ){
        Page<LoginLog> page = loginLogService.page(new Page<>(current, limit),
                new LambdaQueryWrapper<LoginLog>().orderByDesc(LoginLog::getGmtCreated));
        for (LoginLog loginLog : page.getRecords()) {
            loginLog.setName(UserCache.get(loginLog.getUserId()));
        }
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }

    @DeleteMapping("/login/{ids}")
    @Authority("admin:login:log:delete")
    public R loginLogDelete(@PathVariable String ids){
        List<Integer> idList = Arrays.asList(ids.split(",")).stream().map(Integer::parseInt).collect(Collectors.toList());
        loginLogService.removeByIds(idList);
        return R.ok().message("删除成功");
    }
}



