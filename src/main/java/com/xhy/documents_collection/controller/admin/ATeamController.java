package com.xhy.documents_collection.controller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.entity.PO.team.Team;
import com.xhy.documents_collection.entity.PO.team.TeamUser;
import com.xhy.documents_collection.entity.PO.User;
import com.xhy.documents_collection.service.team.TeamService;
import com.xhy.documents_collection.service.team.TeamUserService;
import com.xhy.documents_collection.service.UserService;
import com.xhy.documents_collection.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2022-07-03
 *
 * 团队
 */
@RestController
@RequestMapping("/admin/team")
public class ATeamController {


    @Resource
    private TeamService teamService;

    @Resource
    private UserService userService;


    @Resource
    private TeamUserService teamUserService;


    @GetMapping("/list")
    @Authority("admin:user:list")
    public R list(){
        return R.ok().data("data",teamService.list(new QueryWrapper<Team>().select("id","name")));
    }
    /**
     * 团队列表
     * @param current
     * @param limit
     * @param name
     * @return
     */
    @GetMapping("/page")
    @Authority("admin:team:page")
    public R list(@RequestParam(value = "page") Long current,
                  @RequestParam(value = "limit") Long limit,
                  @RequestParam(required = false) String name){

        Page<Team> page = teamService.page(new Page<>(current, limit),
                new QueryWrapper<Team>().like(!ObjectUtils.isEmpty(name), "name", name));
        // 作缓存表
        Map<Integer, String> userMap = userService.list(new QueryWrapper<User>().select("id", "name")).stream().collect(Collectors.toMap(User::getId, User::getName));
        Map<Integer, Long> teamUserMap = teamUserService.list().stream().collect(Collectors.groupingBy(TeamUser::getTId, Collectors.counting()));
        for (Team team : page.getRecords()) {
            team.setUserName(userMap.get(team.getUId()));
            team.setCount(teamUserMap.get(team.getId()).intValue());
            team.setFileSizeCapacityShow(team.getFileSizeUsed()+"/"+team.getFileSizeCapacity());

        }
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }



    /**
     * 修改团队
     * @param team
     * @return
     */
    @PutMapping
    @Authority("admin:team:update")
    public R update(@RequestBody Team team){
        teamService.updateById(team);
        return R.ok().message("修改团队成功");
    }

    /**
     * 删除团队
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    @Authority("admin:team:delete")
    public R delete(@PathVariable String ids){
        return teamService.removeTeams(ids);
    }



    /**
     * 根据团队id获取团队成员
     * @param id
     * @param current
     * @param limit
     * @param userName
     * @param email
     * @return
     */
    @GetMapping("/getUserPage/{id}")
    @Authority("admin:team:getUserPage")
    public R getUserPage(@PathVariable Integer id,
                         @RequestParam(value = "page")Long current,
                         @RequestParam(value = "limit")Long limit,
                         @RequestParam(required = false) String userName,
                         @RequestParam(required = false) String email
                         ){
        List<Integer> teamIds = teamUserService.list(new QueryWrapper<TeamUser>().eq("t_id", id).select("u_id"))
                .stream()
                .map(TeamUser::getUId)
                .collect(Collectors.toList());
        if (ObjectUtils.isEmpty(teamIds)) {
            return R.ok().data("data", Collections.EMPTY_LIST);
        }
        Page<User> page = userService.page(new Page<>(current, limit), new QueryWrapper<User>().in("id",teamIds)
                .like(!ObjectUtils.isEmpty(userName), "user_name", userName)
                .like(!ObjectUtils.isEmpty(email), "email", email));
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }


}

