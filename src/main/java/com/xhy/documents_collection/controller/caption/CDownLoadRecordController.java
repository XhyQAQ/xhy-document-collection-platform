package com.xhy.documents_collection.controller.caption;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.annotations.OperLog;
import com.xhy.documents_collection.entity.PO.task.Task;
import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.enums.UploadWayEnum;
import com.xhy.documents_collection.entity.PO.task.DownloadRecord;
import com.xhy.documents_collection.service.task.DownloadRecordService;
import com.xhy.documents_collection.service.task.TaskService;
import com.xhy.documents_collection.utils.R;
import com.xhy.documents_collection.utils.TransitionByteToCH;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Author: Xhy
 * CreateTime: 2023-02-07 14:07
 * 下载文件后存储的记录
 */
@RestController
@RequestMapping("/caption/download")
public class CDownLoadRecordController {

    @Resource
    DownloadRecordService downloadRecordService;

    @Resource
    TaskService taskService;

    @GetMapping("/page")
    @Authority("caption:download:page")
    public R page(@RequestParam(value = "page") Long current,
                  @RequestParam(value = "limit") Long limit
    ){
        Integer userId = UserHolder.get();
        Page<DownloadRecord> page = downloadRecordService.page(new Page<>(current, limit), new LambdaQueryWrapper<DownloadRecord>()
                .eq(DownloadRecord::getUserId, userId).orderByDesc(DownloadRecord::getGmtCreated));
        Map<Integer, String> taskMap = taskService.list().stream().collect(Collectors.toMap(Task::getId, Task::getName));
        for (DownloadRecord record : page.getRecords()) {
            record.setFileSizeString(TransitionByteToCH.getPrintSize(record.getFileSize()));
            record.setUploadWayName(UploadWayEnum.getInfo(record.getUploadWay()));
            if (taskMap.containsKey(record.getTaskId())){
                record.setTaskName(taskMap.get(record.getTaskId()));
            }
            record.setFilePath("");
            record.setUrl("");
        }
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }



}
