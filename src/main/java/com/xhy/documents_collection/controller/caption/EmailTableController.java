package com.xhy.documents_collection.controller.caption;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.annotations.OperLog;
import com.xhy.documents_collection.entity.PO.email.EmailTable;
import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.service.EmailTableService;
import com.xhy.documents_collection.utils.R;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2023-03-14
 * 所配置人的邮箱列表
 */
@RestController
@RequestMapping("/caption/email-table")
public class EmailTableController {

    @Resource
    EmailTableService emailTableService;

    @GetMapping("/page")
    @Authority("caption:email:table:page")
    public R page(@RequestParam(value = "page") Long current,
                  @RequestParam(value = "limit") Long limit,
                  @RequestParam(required = false) String description
    ){
        Integer userId = UserHolder.get();
        Page<EmailTable> page = emailTableService.page(new Page<>(current, limit),
                new LambdaQueryWrapper<EmailTable>().like(!ObjectUtils.isEmpty(description), EmailTable::getDescription, description)
        .eq(EmailTable::getUserId,userId));
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }


    @GetMapping("/list")
    @Authority("caption:email:table:list")
    public R list(){
        Integer userId = UserHolder.get();
        List<EmailTable> list = emailTableService.list(new LambdaQueryWrapper<EmailTable>().eq(EmailTable::getUserId, userId));
        return R.ok().data("data",list);
    }

    @PostMapping
    @Authority("caption:email:table:add")
    @OperLog(title = ServiceType.EMAIL,businessType = BusinessType.INSERT)
    public R add(@RequestBody @Validated EmailTable emailTable){

        Integer userId = UserHolder.get();
        emailTable.setUserId(userId);
        emailTableService.save(emailTable);
        return R.ok().message("添加成功");
    }

    @PutMapping
    @Authority("caption:email:table:update")
    @OperLog(title = ServiceType.EMAIL,businessType = BusinessType.UPDATE)
    public R update(@RequestBody @Validated EmailTable emailTable){
        Integer userId = UserHolder.get();
        EmailTable emailTable1 = emailTableService.getById(emailTable.getId());
        if (!emailTable1.getUserId().equals(userId)){
            return R.error().message("你没有权限这样做");
        }
        emailTableService.updateById(emailTable);
        return R.ok().message("修改成功");
    }

    @DeleteMapping("/{ids}")
    @Authority("caption:email:table:delete")
    @OperLog(title = ServiceType.EMAIL,businessType = BusinessType.DELETE)
    public R delete(@PathVariable String ids){
        List<Integer> idList = Arrays.asList(ids.split(",")).stream().map(Integer::parseInt).collect(Collectors.toList());
        if (ObjectUtils.isEmpty(idList)){
            return R.error().message("参数不可为空");
        }
        Integer userId = UserHolder.get();
        emailTableService.remove(new LambdaQueryWrapper<EmailTable>().in(EmailTable::getId,idList).eq(EmailTable::getUserId,userId));
        return R.ok().message("删除成功");
    }
}

