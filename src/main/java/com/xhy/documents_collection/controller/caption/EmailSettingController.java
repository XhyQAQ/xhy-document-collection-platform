package com.xhy.documents_collection.controller.caption;


import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.annotations.OperLog;
import com.xhy.documents_collection.entity.PO.email.EmailSetting;
import com.xhy.documents_collection.enums.BusinessType;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.service.EmailSettingService;
import com.xhy.documents_collection.utils.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xhy
 * @since 2023-03-04
 * 邮箱配置
 */
@RestController
@RequestMapping("/caption/email-setting")
public class EmailSettingController {

    @Resource
    EmailSettingService emailSettingService;

    // 获取当前用户邮箱
    @GetMapping
    @Authority("caption:email:get")
    public R getEmailSetting(){
        return emailSettingService.getByUserId();
    }

    // 修改邮箱配置
    @PutMapping
    @Authority("caption:email:set")
    @OperLog(title = ServiceType.EMAIL_SETTING,businessType = BusinessType.UPDATE)
    public R updateEmailSetting(@RequestBody @Validated EmailSetting emailSetting){
        return emailSettingService.update(emailSetting);
    }

    // 校验邮箱是否可用(用户自己邮箱给自己发送邮件)
    @GetMapping("/testSend")
    @Authority("caption:email:test:send")
    public R testSend(){
        return emailSettingService.testSend();
    }
}

