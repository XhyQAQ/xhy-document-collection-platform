package com.xhy.documents_collection.controller.user;

import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.service.task.GroupService;
import com.xhy.documents_collection.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Author: Xhy
 * CreateTime: 2022-11-24 16:03
 * 组队,已废弃
 */
@RestController
@RequestMapping("/group")
@Deprecated
public class GroupController {
    @Autowired
    private GroupService groupService;

    // 邀请其他人组队
    @PostMapping("/invite")
    @Authority("user:group:invite")
    public R invite(Integer taskId, Integer inviteId){
        return groupService.invite(taskId,inviteId);
    }

    // 取消邀请
    @PostMapping("/cancelInvite")
    @Authority("user:group:cancelInvite")
    public R cancelInvite(Integer taskId,Integer inviteId){
        return groupService.cancelInvite(taskId,inviteId);
    }

    // 退出队伍
    @PostMapping("/exitGroup")
    @Authority("user:group:exitGroup")
    public R exitGroup(Integer taskRecordId){
        return groupService.exitGroup(taskRecordId);
    }
}
