package com.xhy.documents_collection.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.entity.PO.Notice;
import com.xhy.documents_collection.entity.PO.log.FileLog;
import com.xhy.documents_collection.holder.UserHolder;
import com.xhy.documents_collection.service.FileLogService;
import com.xhy.documents_collection.utils.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;

/**
 * Author: Xhy
 * CreateTime: 2023-03-17 10:50
 * 用戶文件操作相关控制器
 */
@RestController
@RequestMapping("/user/file/record")
public class UFileRecordController {


    @Resource
    FileLogService fileLogService;

    @GetMapping("/page")// 加权限
    @Authority("user:file:record:page")
    public R page(@RequestParam(value = "page") Long current,
                  @RequestParam(value = "limit") Long limit
    ) {
        Integer userId = UserHolder.get();
        Page<FileLog> page = fileLogService.page(current, limit, Collections.singleton(userId));
        return R.ok().data("data",page.getRecords()).count(page.getTotal());
    }
}
