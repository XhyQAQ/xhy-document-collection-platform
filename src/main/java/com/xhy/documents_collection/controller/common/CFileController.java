package com.xhy.documents_collection.controller.common;

import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.annotations.Restriction;
import com.xhy.documents_collection.service.oss.OssService;
import com.xhy.documents_collection.utils.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * Author: Xhy
 * CreateTime: 2023-04-04 18:35
 * 文件
 */
@RestController
@RequestMapping("/common/file")
public class CFileController {


    @Resource
    OssService ossService;

    @GetMapping("/download/{id}")
    @Restriction(limit = 8,time = 3600)
    @Authority("common:file:download")
    public R downLoad( @PathVariable Integer id){

        String url = ossService.downloadFile(id);

        return R.ok().data("data",url).message("正在下载中");
    }

}
