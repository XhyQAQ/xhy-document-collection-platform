package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.log.LoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-19
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}
