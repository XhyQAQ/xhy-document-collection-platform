package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.email.EmailSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-04
 */
public interface EmailSettingMapper extends BaseMapper<EmailSetting> {

}
