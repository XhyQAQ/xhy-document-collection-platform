package com.xhy.documents_collection.mapper.team;

import com.xhy.documents_collection.entity.PO.team.Team;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TeamMapper extends BaseMapper<Team> {

}
