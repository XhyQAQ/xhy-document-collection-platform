package com.xhy.documents_collection.mapper.team;

import com.xhy.documents_collection.entity.PO.team.TeamUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface TeamUserMapper extends BaseMapper<TeamUser> {

}
