package com.xhy.documents_collection.mapper.authority;

import com.xhy.documents_collection.entity.PO.authority.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
