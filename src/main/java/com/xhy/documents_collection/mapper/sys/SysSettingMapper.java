package com.xhy.documents_collection.mapper.sys;

import com.xhy.documents_collection.entity.PO.Setting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-16
 */
public interface SysSettingMapper extends BaseMapper<Setting> {

}
