package com.xhy.documents_collection.mapper.sys;

import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-17
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
