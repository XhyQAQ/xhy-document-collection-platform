package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-20
 */
public interface SysNoticeMapper extends BaseMapper<Notice> {

}
