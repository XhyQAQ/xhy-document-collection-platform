package com.xhy.documents_collection.mapper.task;

import com.xhy.documents_collection.entity.PO.task.TaskRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TaskRecordMapper extends BaseMapper<TaskRecord> {

    @Select("select * from task_record where id = #{taskRecordId} limit 1")
    TaskRecord get(String taskRecordId);
}
