package com.xhy.documents_collection.mapper.task;

import com.xhy.documents_collection.entity.PO.task.Task;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface TaskMapper extends BaseMapper<Task> {

    @Select("select t_id from task where id = #{taskId}")
    Integer getTeamIdByTaskId(Integer taskId);

    String getTaskName(Integer taskId);

    @Select("select id,name,description,t_id,u_id,is_group,start_time,end_time,group_user_limit from task where id = #{taskId} and is_deleted = 0 limit 1")
    Task get(Integer taskId);
}
