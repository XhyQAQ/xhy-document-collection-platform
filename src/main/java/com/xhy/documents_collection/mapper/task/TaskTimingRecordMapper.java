package com.xhy.documents_collection.mapper.task;

import com.xhy.documents_collection.entity.PO.task.TaskTimingRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-03
 */
public interface TaskTimingRecordMapper extends BaseMapper<TaskTimingRecord> {

}
