package com.xhy.documents_collection.mapper.task;

import com.xhy.documents_collection.entity.PO.task.TroopUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-22
 */
public interface TroopUserMapper extends BaseMapper<TroopUser> {

}
