package com.xhy.documents_collection.mapper.task;

import com.xhy.documents_collection.entity.PO.task.DownloadRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-02-07
 */
public interface DownloadRecordMapper extends BaseMapper<DownloadRecord> {

}
