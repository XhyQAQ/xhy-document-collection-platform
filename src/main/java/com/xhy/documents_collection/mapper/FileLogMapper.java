package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.log.FileLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-15
 */
public interface FileLogMapper extends BaseMapper<FileLog> {

}
