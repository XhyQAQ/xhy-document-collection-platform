package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.email.EmailTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2023-03-14
 */
public interface EmailTableMapper extends BaseMapper<EmailTable> {

}
