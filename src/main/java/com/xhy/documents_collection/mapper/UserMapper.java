package com.xhy.documents_collection.mapper;

import com.xhy.documents_collection.entity.PO.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xhy
 * @since 2022-11-14
 */
public interface UserMapper extends BaseMapper<User> {

    @Select("select id,name,password,email from user where email = #{email} and is_deleted = 0 limit 1")
    User getUserByEmail(String email);

    @Select("select id,name,password,email from user where student_number = #{studentNumber} and is_deleted = 0 limit 1")
    User getUserByStudentNumber(String studentNumber);
}
