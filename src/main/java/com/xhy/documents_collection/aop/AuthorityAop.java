package com.xhy.documents_collection.aop;


import com.xhy.documents_collection.annotations.Authority;
import com.xhy.documents_collection.authority.AuthorityUtils;
import com.xhy.documents_collection.authority.UnifyAuthorityVerify;

import com.xhy.documents_collection.exception.BaseException;
import com.xhy.documents_collection.utils.JwtUtils;
import com.xhy.documents_collection.utils.SpringUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Author: Xhy
 * CreateTime: 2022-06-05 00:36
 * 权限校验
 * 多个aop的情况下使用 @Order(1) 来指定顺序
 */

@Aspect
@Component
public class AuthorityAop {



    @Autowired
    private HttpServletRequest request;





    /**
     * 使用 @PostMapping 作为全局校验aop
     * 实现类bean的名称必须是 postMappingAuthorityVerify
     * 使用该aop需要把过滤的请求放入 AuthorityUtils
     * @param joinPoint
     * @param authority
     * @return
     * @throws Throwable
     */
    @Around("@annotation(authority)")
    public Object postAuthority(ProceedingJoinPoint joinPoint, PostMapping authority) throws Throwable {

        if (AuthorityUtils.getPostAuthority()){
            String requestURI = request.getRequestURI();
            if (!AuthorityUtils.filterPermission(requestURI)){
                UnifyAuthorityVerify unifyAuthorityVerify = (UnifyAuthorityVerify) SpringUtil.getBean("postMappingAuthorityVerify");
                if (!ObjectUtils.isEmpty(unifyAuthorityVerify)){
                    Boolean result = unifyAuthorityVerify.authorityVerify(request,requestURI);
                    if (!result) throw new BaseException(403,"权限不足");
                }
            }
        }

        Object data = joinPoint.proceed();
        return data;
    }

    /**
     * 自定义校验aop
     * @param joinPoint
     * @param authority
     * @return
     * @throws Throwable
     */
    @Around("@annotation(authority)")
    public Object authority(ProceedingJoinPoint joinPoint, Authority authority) throws Throwable {
        Boolean result;
        Method method;
        Object verifyObject;

        if (!AuthorityUtils.getPostAuthority()){
            // 全局校验类
            Class globalVerify = AuthorityUtils.getGlobalVerify();
            verifyObject = globalVerify.newInstance();
            method = globalVerify.getMethod("authorityVerify", HttpServletRequest.class, String[].class);
            result = (Boolean) method.invoke(verifyObject, request,authority.value());
            if (!result)  throw new BaseException(403,"权限不足");
        }
        Object o = joinPoint.proceed();
        return o;
    }


}
