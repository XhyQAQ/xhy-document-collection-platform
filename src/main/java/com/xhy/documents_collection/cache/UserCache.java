package com.xhy.documents_collection.cache;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhy.documents_collection.entity.PO.log.SysLog;
import com.xhy.documents_collection.entity.PO.User;
import com.xhy.documents_collection.enums.ServiceType;
import com.xhy.documents_collection.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Author: Xhy
 * CreateTime: 2022-11-21 11:21
 *
 * 用户缓存表
 * 用于数据关联时减少数据库的查询操作
 */
@Component
public class UserCache {

    // key: userId  value:username
    private static Map<Integer,String> userCache ;

    public static void put(Map<Integer,String> userMap){
        userCache = userMap;
    }

    public static void setValue(Integer id,String username){
        if (userCache.containsKey(id)){
            userCache.put(id,username);
        }
    }

    public static void set(Map<Integer,String> userMap){
        userCache = userMap;
    }

    public static void clear(){
        userCache.clear();
    }

    public static void refresh(){
        final Map<Integer, String> userMap = userService.list().stream().collect(Collectors.toMap(User::getId, User::getName));
        userCache = userMap;
    }
    public static String get(Integer id){
        return userCache.containsKey(id) ? userCache.get(id) : userService.getById(id).getName();
    }

    public static List<String> list(List<Integer> userIds){

        List<String> names = userIds.stream().map(userId -> userCache.get(userId)).collect(Collectors.toList());
        return names;
    }


    private static UserService userService;

    @Autowired
    public void setSysLogService(UserService userService){
        UserCache.userService = userService;
        // 填充缓存
        Map<Integer, String> userMap = userService.list(new LambdaQueryWrapper<User>().select(User::getId, User::getName))
                .stream().collect(Collectors.toMap(User::getId, User::getName));
        put(userMap);
    }
}
