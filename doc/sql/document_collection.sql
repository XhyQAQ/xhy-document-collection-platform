/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 50536
Source Host           : 127.0.0.1:3306
Source Database       : document_collection

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2023-04-14 21:37:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for download_record
-- ----------------------------
DROP TABLE IF EXISTS `download_record`;
CREATE TABLE `download_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `file_size` bigint(20) NOT NULL DEFAULT '0',
  `size` int(11) NOT NULL DEFAULT '0',
  `upload_way` tinyint(1) DEFAULT '0' COMMENT '0为手动下载，1为定时下载，默认为0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `download_size` int(11) DEFAULT 0,
  `file_path` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of download_record
-- ----------------------------

-- ----------------------------
-- Table structure for email_setting
-- ----------------------------
DROP TABLE IF EXISTS `email_setting`;
CREATE TABLE `email_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(20) NOT NULL DEFAULT '',
  `user_name` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of email_setting
-- ----------------------------
INSERT INTO `email_setting` VALUES ('7', '', '', '', '10111', '0', '0');

-- ----------------------------
-- Table structure for email_table
-- ----------------------------
DROP TABLE IF EXISTS `email_table`;
CREATE TABLE `email_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of email_table
-- ----------------------------

-- ----------------------------
-- Table structure for file_log
-- ----------------------------
DROP TABLE IF EXISTS `file_log`;
CREATE TABLE `file_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  `content` varchar(255) NOT NULL DEFAULT '',
  `u_id` int(11) NOT NULL,
  `business_type` tinyint(4) NOT NULL,
  `level` tinyint(1) DEFAULT NULL,
  `file_size` varchar(20) NOT NULL DEFAULT '',
  `file_name` varchar(100) NOT NULL DEFAULT '',
  `task_record_id` int(11) DEFAULT NULL,
  `state` tinyint(4) DEFAULT '0',
  `ip` varchar(30) NOT NULL DEFAULT '',
  `os` varchar(20) NOT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of file_log
-- ----------------------------

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `content` varchar(20) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(30) DEFAULT NULL,
  `browser` varchar(20) DEFAULT NULL,
  `operating_system` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4139 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of login_log
-- ----------------------------


-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `href` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_menu` tinyint(2) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '1',
  `state` tinyint(4) DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '0', null, null, 'fa fa-align-justify', '业务管理', null, '-1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('2', '1', 'system:main', 'page/welcome.html', 'fa fa-home', '主页', '', '0', '_self', '10', '0', '1', null, '2022-11-16 15:06:02');
INSERT INTO `permission` VALUES ('3', '1', '', '', 'fa fa-user', '用户管理', null, '-1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('4', '3', '', 'page/user.html', 'fa fa-user-o', '用户', null, '0', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('32', '0', '', '', 'fa fa-gears', '系统设置', null, '-1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('61', '32', '', '', 'fa fa-gears', '系统管理', null, '-1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('62', '61', 'admin:system:get', 'page/setting.html', 'fa fa-gear', '系统设置', '', '0', null, '1', '0', '0', null, '2022-11-24 11:44:40');
INSERT INTO `permission` VALUES ('69', '62', 'system:update', '', 'fa fa-gear', '修改系统设置', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('72', '62', 'system:clearCache', '', 'fa fa-trash', '清理缓存', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('73', '32', '', '', 'fa fa-shield', '权限管理', null, '-1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('74', '73', 'permission:list', 'page/permission.html', 'fa fa-shield', '权限', null, '0', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('75', '74', 'permission:add', '', 'fa fa-save', '添加权限', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('76', '74', 'permission:update', '', 'fa fa-gear', '修改权限', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('77', '74', 'permission:delete', '', 'fa fa-remove', '删除权限', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('78', '74', 'permission:treeSelect', '', 'fa fa-list', '树形下拉选择框', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('79', '74', 'permission:treeList', '', 'fa fa-tree', '树形', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('80', '73', 'role:list', 'page/role.html', 'fa fa-user-circle', '角色管理', null, '0', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('81', '80', 'role:add', '', 'fa fa-save', '添加角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('82', '80', 'role:update', '', 'fa fa-gear', '修改角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('84', '80', 'role:delete', '', 'fa fa-remove', '删除角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('85', '80', 'role:authority', '', 'fa fa-user-secret', '分配权限', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('86', '80', 'role:getPermission', '', 'fa fa-info', '获取角色权限', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('87', '89', 'role:getRole', '', 'fa fa-info', '获取用户角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('88', '89', 'role:initRole', '', 'fa fa-user-o', '获取角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('89', '73', 'user:list', 'page/user-role.html', 'fa fa-user-o', '分配角色', null, '0', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('90', '4', 'admin:user:add', '', 'fa fa-save', '添加用户', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('91', '89', 'user:assignRole', '', 'fa fa-info', '分配用户角色', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('92', '4', 'admin:user:update', '', 'fa fa-gear', '编辑用户', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('93', '4', 'admin:user:delete', '', 'fa fa-remove', '删除用户', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('104', '2', 'system:echarts', '', 'fa fa-info', 'echarts图', null, '1', null, '1', '0', '1', null, null);
INSERT INTO `permission` VALUES ('107', '4', 'admin:user:page', '', 'fa fa-list', '用户列表(带分页)', null, '1', null, '1', '0', '0', null, null);
INSERT INTO `permission` VALUES ('121', '1', '', '', 'fa fa-tasks', '任务管理', null, '-1', null, '1', '0', '1', '2022-07-05 21:15:00', null);
INSERT INTO `permission` VALUES ('122', '126', 'admin:task:list', '', 'fa fa-list', '任务列表', null, '1', null, '1', '0', '1', '2022-07-05 21:15:39', null);
INSERT INTO `permission` VALUES ('123', '126', 'admin:task:add', '', 'fa fa-save', '添加任务', null, '1', null, '1', '0', '1', '2022-07-05 21:16:01', null);
INSERT INTO `permission` VALUES ('124', '126', 'admin:task:update', '', 'fa fa-gear', '修改任务', null, '1', null, '1', '0', '1', '2022-07-05 21:16:48', null);
INSERT INTO `permission` VALUES ('125', '126', 'admin:task:delete', '', 'fa fa-remove', '删除任务', null, '1', null, '1', '0', '1', '2022-07-05 21:17:03', null);
INSERT INTO `permission` VALUES ('126', '121', '', 'page/task.html', 'fa fa-tasks', '任务', null, '0', null, '1', '0', '1', '2022-07-05 21:22:30', null);
INSERT INTO `permission` VALUES ('127', '1', '', '', 'fa fa-group', '团队管理', null, '-1', null, '1', '0', '0', '2022-07-05 21:28:32', null);
INSERT INTO `permission` VALUES ('128', '127', '', 'page/team.html', 'fa ', '团队', null, '0', null, '1', '0', '0', '2022-07-05 21:29:03', null);
INSERT INTO `permission` VALUES ('129', '128', 'admin:team:page', '', 'fa fa-list', '团队列表(带分页)', null, '1', null, '1', '0', '0', '2022-07-05 21:29:40', null);
INSERT INTO `permission` VALUES ('130', '128', 'admin:team:add', '', 'fa fa-save', '添加团队', null, '1', null, '1', '0', '0', '2022-07-05 21:30:37', null);
INSERT INTO `permission` VALUES ('131', '128', 'admin:team:update', '', 'fa fa-gear', '修改团队', null, '1', null, '1', '0', '0', '2022-07-05 21:30:55', null);
INSERT INTO `permission` VALUES ('132', '128', 'admin:team:delete', '', 'fa fa-remove', '删除团队', null, '1', null, '1', '0', '0', '2022-07-05 21:31:13', null);
INSERT INTO `permission` VALUES ('133', '4', 'admin:user:list', '', 'fa fa-list', '用户列表', null, '1', null, '1', '0', '0', '2022-07-06 00:22:21', null);
INSERT INTO `permission` VALUES ('134', '4', 'admin:user:list', '', 'fa fa-list', '用户列表', null, '1', null, '1', '0', '0', '2022-07-06 00:22:21', null);
INSERT INTO `permission` VALUES ('135', '128', 'admin:team:list', '', 'fa fa-list', '团队列表', null, '1', null, '1', '0', '0', '2022-07-06 00:25:49', null);
INSERT INTO `permission` VALUES ('136', '126', 'admin:task:record:list', 'page/task-record.html', 'fa fa-list', '任务记录列表', null, '1', null, '1', '0', '1', '2022-07-06 17:08:22', null);
INSERT INTO `permission` VALUES ('137', '126', 'admin:task:record:add', '', 'fa fa-save', '添加任务记录', null, '1', null, '1', '0', '1', '2022-07-06 17:08:52', null);
INSERT INTO `permission` VALUES ('138', '128', 'admin:team:getUserPage', 'page/team-user.html', 'fa fa-list', '团队成员信息', null, '1', null, '1', '0', '0', '2022-07-06 17:09:46', null);
INSERT INTO `permission` VALUES ('139', '128', 'admin:team:addUser', '', 'fa fa-save', '添加团队成员', null, '1', null, '1', '0', '0', '2022-07-06 17:26:32', null);
INSERT INTO `permission` VALUES ('140', '128', 'admin:team:deleteUser', '', 'fa fa-remove', '移出团队成员', null, '1', null, '1', '0', '0', '2022-07-06 17:28:08', null);
INSERT INTO `permission` VALUES ('151', '1', '', '', 'fa fa-crosshairs', 'OSS管理', null, '-1', null, '1', '0', '1', '2022-07-08 21:36:50', null);
INSERT INTO `permission` VALUES ('152', '151', '', 'page/team.html', 'fa ', 'OSS', null, '0', null, '1', '0', '1', '2022-07-08 21:37:11', null);
INSERT INTO `permission` VALUES ('153', '152', '', '', 'fa fa-list', 'OSS记录列表', null, '1', null, '1', '0', '1', '2022-07-08 21:56:35', null);
INSERT INTO `permission` VALUES ('154', '152', 'admin:oss:download', '', 'fa fa-cloud-download', '下载文件', null, '1', null, '1', '0', '1', '2022-07-08 21:57:08', null);
INSERT INTO `permission` VALUES ('155', '152', 'admin:oss:upload', '', 'fa fa-cloud-upload', '上传文件', null, '1', null, '1', '0', '1', '2022-07-08 21:57:26', null);
INSERT INTO `permission` VALUES ('156', '152', 'admin:oss:delete', '', 'fa fa-remove', '删除文件', null, '1', null, '1', '0', '1', '2022-07-08 21:57:58', null);
INSERT INTO `permission` VALUES ('157', '0', '', '', 'fa fa-info', '后台管理', '团队队长的后台管理界面', '-1', null, '1', '0', '1', '2022-07-08 23:17:41', null);
INSERT INTO `permission` VALUES ('158', '157', '', '', 'fa fa-group', '团队管理', '团队管理', '-1', null, '1', '0', '1', '2022-07-08 23:19:30', null);
INSERT INTO `permission` VALUES ('159', '158', 'caption:team:page', 'page/caption/team.html', 'fa ', '团队', '', '0', null, '1', '0', '1', '2022-07-08 23:20:44', null);
INSERT INTO `permission` VALUES ('160', '159', 'caption:team:get', '', 'fa fa-info', '获取团队详细信息', '', '1', null, '1', '0', '1', '2022-07-08 23:21:18', null);
INSERT INTO `permission` VALUES ('161', '159', 'caption:team:add', '', 'fa fa-save', '添加团队', '', '1', null, '1', '0', '1', '2022-07-08 23:21:34', null);
INSERT INTO `permission` VALUES ('162', '159', 'caption:team:update', '', 'fa fa-gear', '修改团队', '', '1', null, '1', '0', '1', '2022-07-08 23:21:57', null);
INSERT INTO `permission` VALUES ('163', '159', 'caption:team:delete', '', 'fa fa-remove', '删除团队', '', '1', null, '1', '0', '1', '2022-07-08 23:22:15', null);
INSERT INTO `permission` VALUES ('164', '159', 'caption:team:getUserPage', '', 'fa fa-list', '获取团队成员', '', '1', null, '1', '0', '1', '2022-07-08 23:57:56', null);
INSERT INTO `permission` VALUES ('165', '159', 'caption:team:addUser', '', 'fa fa-save', '添加团队成员', '添加团队成员,队长需要开通邮箱服务 TODO ', '1', null, '1', '0', '1', '2022-07-08 23:58:29', null);
INSERT INTO `permission` VALUES ('166', '159', 'caption:team:deleteUser', '', 'fa fa-save', '移出团队', '', '1', null, '1', '0', '1', '2022-07-08 23:58:42', null);
INSERT INTO `permission` VALUES ('167', '157', '', '', 'fa fa-tasks', '任务管理', '', '-1', null, '1', '0', '1', '2022-07-09 01:03:45', null);
INSERT INTO `permission` VALUES ('168', '167', '', 'page/caption/task.html', 'fa fa-tasks', '任务', '', '0', null, '1', '0', '1', '2022-07-09 21:30:27', null);
INSERT INTO `permission` VALUES ('169', '168', 'caption:task:page', '', 'fa fa-list', '任务列表', '', '1', null, '1', '0', '1', '2022-07-09 21:30:51', null);
INSERT INTO `permission` VALUES ('170', '168', 'caption:task:add', '', 'fa fa-save', '新增任务', '', '1', null, '1', '0', '1', '2022-07-09 21:31:34', null);
INSERT INTO `permission` VALUES ('171', '168', 'caption:task:update', '', 'fa fa-gear', '修改任务', '', '1', null, '1', '0', '1', '2022-07-09 21:31:55', null);
INSERT INTO `permission` VALUES ('172', '168', 'caption:task:delete', '', 'fa fa-remove', '删除任务', '', '1', null, '1', '0', '1', '2022-07-09 21:32:12', null);
INSERT INTO `permission` VALUES ('173', '168', 'caption:task:record:list', '', 'fa fa-list', '任务记录列表', '', '1', null, '1', '0', '1', '2022-07-09 21:34:44', null);
INSERT INTO `permission` VALUES ('174', '168', 'caption:task:record:add', '', 'fa fa-save', '添加任务记录', '', '1', null, '1', '0', '1', '2022-07-09 21:34:58', null);
INSERT INTO `permission` VALUES ('175', '168', 'caption:oss:download', '', 'fa fa-cloud-download', '下载文件', '', '1', null, '1', '0', '1', '2022-07-09 22:47:04', null);
INSERT INTO `permission` VALUES ('176', '168', 'caption:oss:upload', '', 'fa fa-cloud-upload', '上传文件', '', '1', null, '1', '0', '1', '2022-07-09 22:47:20', null);
INSERT INTO `permission` VALUES ('177', '168', 'caption:oss:uploadPicture', '', 'fa fa-cloud-upload', '上传图片', '', '1', null, '1', '0', '1', '2022-07-09 22:47:35', null);
INSERT INTO `permission` VALUES ('178', '168', 'caption:oss:delete', '', 'fa fa-remove', '删除文件', '', '1', null, '1', '0', '1', '2022-07-09 22:48:02', null);
INSERT INTO `permission` VALUES ('179', '168', 'caption:email:send', '', 'fa fa-send', '发送邮箱', '', '1', null, '1', '0', '1', '2022-07-09 22:49:25', null);
INSERT INTO `permission` VALUES ('180', '126', 'admin:email:send', '', 'fa fa-send', '发送邮箱', '', '1', null, '1', '0', '1', '2022-07-09 22:50:33', null);
INSERT INTO `permission` VALUES ('181', '159', 'caption:user:list', '', 'fa fa-list', '团队列表', '团队队长所拥有的团队', '1', null, '1', '0', '1', '2022-07-10 18:52:34', null);
INSERT INTO `permission` VALUES ('182', '159', 'caption:batch:user', '', 'fa fa-save', '批量添加用户至团队', '使用excel批量添加用户到团队', '1', null, '1', '0', '1', '2022-07-10 22:42:44', null);
INSERT INTO `permission` VALUES ('183', '0', '', '', 'fa fa-info', '用户管理', '用户所拥有的权限', '-1', null, '1', '0', '1', '2022-07-13 16:46:44', null);
INSERT INTO `permission` VALUES ('184', '183', '', '', 'fa fa-group', '团队列表', '用户所拥有的团队', '-1', null, '1', '0', '1', '2022-07-13 16:47:19', null);
INSERT INTO `permission` VALUES ('185', '184', '', 'page/user/team.html', 'fa ', '团队', '团队列表', '0', null, '1', '0', '1', '2022-07-13 16:47:48', null);
INSERT INTO `permission` VALUES ('186', '185', 'user:team:page', '', 'fa fa-list', '团队', '', '1', null, '1', '0', '1', '2022-07-13 16:49:47', null);
INSERT INTO `permission` VALUES ('187', '185', 'user:team:add', '', 'fa fa-save', '加入团队', '用户加入团队', '1', null, '1', '0', '1', '2022-07-13 16:50:27', null);
INSERT INTO `permission` VALUES ('188', '185', 'user:task:record:list', 'page/user/task-record.html', 'fa fa-info', '获取用户的某个团队发起的所有任务', '', '1', null, '1', '0', '1', '2022-07-13 18:23:50', null);
INSERT INTO `permission` VALUES ('189', '185', 'user:oss:upload', '', 'fa fa-cloud-upload', '上传文件', '', '1', null, '1', '0', '1', '2022-07-13 18:24:21', null);
INSERT INTO `permission` VALUES ('190', '185', 'user:oss:uploadPicture', '', 'fa fa-cloud-upload', '上传图片', '', '1', null, '1', '0', '1', '2022-07-13 18:24:36', null);
INSERT INTO `permission` VALUES ('191', '32', '', '', 'fa fa-info', '公告管理', '公告', '-1', null, '1', '0', '1', '2022-07-13 21:12:54', null);
INSERT INTO `permission` VALUES ('192', '191', '', 'page/notice.html', 'fa fa-info', '公告', '', '0', null, '1', '0', '1', '2022-07-13 21:13:27', null);
INSERT INTO `permission` VALUES ('193', '192', 'admin:notice:page', '', 'fa fa-list', '公告列表', '', '1', null, '1', '0', '1', '2022-07-13 21:14:01', null);
INSERT INTO `permission` VALUES ('194', '192', 'admin:notice:update', '', 'fa fa-gear', '修改公告', '', '1', null, '1', '0', '1', '2022-07-13 21:14:13', null);
INSERT INTO `permission` VALUES ('195', '192', 'admin:notice:delete', '', 'fa fa-remove', '删除公告', '', '1', null, '1', '0', '1', '2022-07-13 21:14:25', null);
INSERT INTO `permission` VALUES ('196', '183', 'user:main', 'page/user/main.html', 'fa fa-tachometer', '主页', '', '0', null, '9', '0', '1', '2022-07-13 22:03:20', null);
INSERT INTO `permission` VALUES ('197', '0', 'common:user:update', 'page/user-setting.html', 'fa fa-gear', '修改密码', '公共接口', '1', null, '1', '0', '0', '2022-07-13 22:46:26', null);
INSERT INTO `permission` VALUES ('198', '0', 'common:user:get', '', 'fa fa-user', '获取用户信息', '公告接口', '1', null, '1', '0', '0', '2022-07-13 22:51:23', null);
INSERT INTO `permission` VALUES ('199', '157', '', 'page/caption/email-setting.html', 'fa fa-send', '邮箱配置', '邮箱配置', '-1', null, '1', '0', '1', '2022-07-13 22:53:04', null);
INSERT INTO `permission` VALUES ('200', '199', 'caption:email:get', '', 'fa fa-info', '获取邮箱配置', '', '1', null, '1', '0', '1', '2022-07-13 22:53:34', null);
INSERT INTO `permission` VALUES ('201', '199', 'caption:email:set', '', 'fa fa-gear', '修改邮箱配置', '', '1', null, '1', '0', '1', '2022-07-13 22:54:06', null);
INSERT INTO `permission` VALUES ('202', '157', 'caption:main', 'page/caption/main.html', 'fa fa-tachometer', '主页', '团队队长的主页', '-1', null, '9', '0', '1', '2022-07-13 23:10:15', null);
INSERT INTO `permission` VALUES ('203', '183', 'common:apply:caption', '', 'fa fa-graduation-cap', '申请团队队长', '申请团队队长', '1', null, '1', '0', '1', '2022-07-13 23:38:54', null);
INSERT INTO `permission` VALUES ('204', '192', 'admin:notice:add', '', 'fa fa-save', '发布公告', '', '1', null, '1', '0', '1', '2022-07-17 16:14:48', null);
INSERT INTO `permission` VALUES ('205', '0', 'common:upload', '', 'fa fa-cloud-upload', '上传文件', '上传文件公共接口', '1', null, '1', '0', '1', '2022-08-29 18:34:41', null);
INSERT INTO `permission` VALUES ('206', '32', '', '', 'fa fa-info', '日志管理', '日志', '-1', null, '2', '0', '0', '2022-11-17 21:11:26', '2022-11-17 22:52:50');
INSERT INTO `permission` VALUES ('207', '206', '', 'page/oper-log.html', 'fa fa-info', '操作日志', '日志', '0', null, '1', '0', '0', '2022-11-17 21:13:01', '2023-03-16 18:26:30');
INSERT INTO `permission` VALUES ('208', '207', 'admin:oper:log:delete', '', 'fa fa-remove', '删除日志', '删除日志', '1', null, '1', '0', '0', '2022-11-17 21:13:30', '2023-03-20 09:56:22');
INSERT INTO `permission` VALUES ('209', '207', 'admin:oper:log:page', '', 'fa fa-info', '查看日志', '查看日志', '1', null, '1', '0', '0', '2022-11-17 22:55:16', '2023-03-20 09:57:48');
INSERT INTO `permission` VALUES ('210', '0', '', '', 'fa ', '负责人后台管理', '团队队长后台管理', '-1', null, '1', '0', '0', '2022-11-18 09:01:29', '2023-03-04 23:42:47');
INSERT INTO `permission` VALUES ('211', '0', '', 'page/caption/main.html', 'fa ', '主页', '团队队长主页信息', '-1', null, '1', '0', '1', '2022-11-18 09:03:30', '2022-11-18 16:09:12');
INSERT INTO `permission` VALUES ('212', '210', '', '', 'fa fa-group', '团队管理', '团队管理信息', '-1', null, '1', '0', '0', '2022-11-18 16:07:29', '2022-11-18 16:07:29');
INSERT INTO `permission` VALUES ('213', '212', 'caption:team:page', 'page/caption/team.html', 'fa ', '团队', '团队信息', '0', null, '1', '0', '0', '2022-11-18 16:08:52', '2022-11-18 16:08:52');
INSERT INTO `permission` VALUES ('214', '213', 'caption:team:add', '', 'fa fa-save', '添加团队', '添加团队', '1', null, '1', '0', '0', '2022-11-18 16:10:03', '2022-11-18 16:10:03');
INSERT INTO `permission` VALUES ('215', '213', 'caption:team:update', '', 'fa fa-gear', '修改团队', '修改团队', '1', null, '1', '0', '0', '2022-11-18 16:10:37', '2022-11-18 16:10:37');
INSERT INTO `permission` VALUES ('216', '213', 'caption:team:delete', '', 'fa fa-remove', '删除团队', '删除团队', '1', null, '1', '0', '0', '2022-11-18 16:10:56', '2022-11-18 16:10:56');
INSERT INTO `permission` VALUES ('217', '213', 'caption:team:user:page', '', 'fa ', '获取团队人员信息', '获取团队人员信息', '1', null, '1', '0', '0', '2022-11-18 17:27:58', '2022-11-18 17:27:58');
INSERT INTO `permission` VALUES ('218', '213', 'caption:team:user:add', '', 'fa ', '添加团队成员', '', '1', null, '1', '0', '0', '2022-11-18 17:28:19', '2022-11-18 17:28:19');
INSERT INTO `permission` VALUES ('219', '213', 'caption:team:user:delete', '', 'fa ', '删除团队人员', '', '1', null, '1', '0', '0', '2022-11-18 17:28:33', '2022-11-18 17:28:33');
INSERT INTO `permission` VALUES ('220', '213', 'caption:team:batch:user', '', 'fa ', '批量添加用户', '使用excel批量添加用户', '1', null, '1', '0', '0', '2022-11-18 23:58:34', '2022-11-18 23:58:34');
INSERT INTO `permission` VALUES ('221', '210', '', '', 'fa fa-tasks', '任务管理', '任务', '-1', null, '1', '0', '0', '2022-11-19 19:45:37', '2022-11-19 19:45:37');
INSERT INTO `permission` VALUES ('222', '221', 'caption:task:page', 'page/caption/task.html', 'fa fa-tasks', '任务', '', '0', null, '1', '0', '0', '2022-11-19 19:46:06', '2022-11-19 19:46:29');
INSERT INTO `permission` VALUES ('223', '221', 'caption:task:add', '', 'fa fa-save', '发布任务', '', '1', null, '1', '0', '0', '2022-11-19 19:46:50', '2022-11-19 19:46:50');
INSERT INTO `permission` VALUES ('224', '221', 'caption:task:update', '', 'fa fa-gear', '修改任务', '', '1', null, '1', '0', '0', '2022-11-19 19:47:07', '2022-11-19 19:47:07');
INSERT INTO `permission` VALUES ('225', '221', 'caption:task:delete', '', 'fa fa-remove', '删除任务', '', '1', null, '1', '0', '0', '2022-11-19 19:47:24', '2022-11-19 19:47:24');
INSERT INTO `permission` VALUES ('226', '221', 'caption:task:page', '', 'fa fa-info', '获取任务下的所有记录', '', '1', null, '1', '0', '0', '2022-11-19 19:47:55', '2022-11-19 19:47:55');
INSERT INTO `permission` VALUES ('227', '213', 'caption:team:list', '', 'fa fa-info', '团队列表', '', '1', null, '1', '0', '0', '2022-11-19 19:48:43', '2022-11-19 19:48:43');
INSERT INTO `permission` VALUES ('228', '222', 'caption:task:upload', '', 'fa fa-cloud-upload', '上传文件', '团队队长上传文件', '1', null, '1', '0', '0', '2022-11-20 15:44:26', '2022-11-20 15:44:26');
INSERT INTO `permission` VALUES ('229', '222', 'caption:task:sendBack', '', 'fa fa-remove', '退回文件', '团队队长退回文件', '1', null, '1', '0', '0', '2022-11-20 15:44:50', '2022-11-20 16:30:47');
INSERT INTO `permission` VALUES ('230', '222', 'caption:task:download', '', 'fa fa-cloud-download', '下载文件', '下载文件成压缩包', '1', null, '1', '0', '0', '2022-11-20 17:57:25', '2022-11-20 17:57:25');
INSERT INTO `permission` VALUES ('231', '0', '', '', 'fa ', '用户后台管理', '普通用户后台管理', '-1', null, '1', '0', '0', '2022-11-21 12:03:56', '2023-03-04 23:42:40');
INSERT INTO `permission` VALUES ('232', '231', 'user:team:page', '/page/user/team.html', 'fa fa-group', '团队', '', '-1', null, '1', '0', '0', '2022-11-21 12:05:05', '2022-11-21 12:05:58');
INSERT INTO `permission` VALUES ('233', '231', 'user:task:page', '/page/user/task.html', 'fa fa-tasks', '任务', '', '-1', null, '1', '0', '0', '2022-11-21 12:05:50', '2022-11-21 12:05:50');
INSERT INTO `permission` VALUES ('234', '62', 'admin:system:save', '', 'fa fa-gear', '保存系统设置', '', '1', null, '1', '0', '0', '2022-11-24 11:45:18', '2022-11-24 11:45:18');
INSERT INTO `permission` VALUES ('235', '128', 'admin:team:getUserPage', 'page/team-user.html', 'fa ', '根据团队id获取团队成员', '', '1', null, '1', '0', '0', '2022-11-24 12:43:16', '2022-11-24 12:43:16');
INSERT INTO `permission` VALUES ('236', '233', 'user:group:invite', '', 'fa ', '邀请其他人组队', '', '1', null, '1', '0', '0', '2022-11-24 16:05:39', '2022-11-24 16:05:39');
INSERT INTO `permission` VALUES ('237', '233', 'user:group:cancelInvite', '', 'fa ', '取消邀请', '', '1', null, '1', '0', '0', '2022-11-24 16:05:48', '2022-11-24 16:05:48');
INSERT INTO `permission` VALUES ('238', '233', 'user:group:exitGroup', '', 'fa ', '退出队伍', '', '1', null, '1', '0', '0', '2022-11-24 16:05:59', '2022-11-24 16:05:59');
INSERT INTO `permission` VALUES ('239', '221', 'caption:download:page', 'page/caption/download-record.html', 'fa fa-cloud-download', '下载记录', '', '0', null, '1', '0', '0', '2023-02-07 14:50:47', '2023-02-07 14:59:06');
INSERT INTO `permission` VALUES ('240', '210', '', 'page/caption/email-setting.html', 'fa fa-send', '邮箱配置', '邮箱配置', '-1', null, '2', '0', '0', '2023-03-04 23:49:49', '2023-03-04 23:53:35');
INSERT INTO `permission` VALUES ('241', '240', 'caption:email:get', '', 'fa fa-info', '获取邮箱配置', '获取邮箱配置', '1', null, '1', '0', '0', '2023-03-04 23:50:27', '2023-03-04 23:50:27');
INSERT INTO `permission` VALUES ('242', '240', 'caption:email:set', '', 'fa fa-info', '修改邮箱配置', '', '1', null, '1', '0', '0', '2023-03-04 23:50:41', '2023-03-04 23:50:41');
INSERT INTO `permission` VALUES ('243', '240', 'caption:email:test:send', '', 'fa fa-info', '测试邮箱', '校验邮箱是否可用(用户自己邮箱给自己发送邮件)', '1', null, '1', '0', '0', '2023-03-04 23:51:03', '2023-03-04 23:51:03');
INSERT INTO `permission` VALUES ('244', '210', 'caption:timing:page', '', 'fa fa-calendar', '定时任务', '定时任务', '-1', null, '3', '0', '0', '2023-03-05 10:23:07', '2023-03-14 15:35:23');
INSERT INTO `permission` VALUES ('245', '252', 'caption:timing:add', '', 'fa fa-save', '添加定时任务', '', '1', null, '1', '0', '0', '2023-03-05 10:23:29', '2023-03-14 15:38:15');
INSERT INTO `permission` VALUES ('246', '252', 'caption:timing:update', '', 'fa fa-info', '修改定时任务', '', '1', null, '1', '0', '0', '2023-03-05 10:24:25', '2023-03-14 15:38:25');
INSERT INTO `permission` VALUES ('247', '252', 'caption:timing:delete', '', 'fa fa-remove', '删除定时任务', '', '1', null, '1', '0', '0', '2023-03-05 10:24:46', '2023-03-14 15:38:34');
INSERT INTO `permission` VALUES ('248', '221', 'caption:task:listNoIncludeTiming', '', 'fa fa-info', '获取没有定时的任务', '', '1', null, '1', '0', '0', '2023-03-05 10:56:01', '2023-03-05 10:56:01');
INSERT INTO `permission` VALUES ('249', '233', 'user:task:get', '', 'fa ', '查看任务详情 -> 提交记录表', '', '1', null, '1', '0', '0', '2023-03-05 22:02:43', '2023-03-05 22:02:43');
INSERT INTO `permission` VALUES ('250', '233', 'user:task:upload', '', 'fa ', '上传任务', '', '1', null, '1', '0', '0', '2023-03-05 22:02:56', '2023-03-05 22:02:56');
INSERT INTO `permission` VALUES ('251', '233', 'user:task:back', '', 'fa ', '撤回任务', '', '1', null, '1', '0', '0', '2023-03-05 22:03:08', '2023-03-05 22:03:08');
INSERT INTO `permission` VALUES ('252', '244', 'caption:timing:page', 'page/caption/timing.html', 'fa fa-calendar', '定时任务', '', '0', null, '1', '0', '0', '2023-03-14 15:34:40', '2023-03-14 15:35:11');
INSERT INTO `permission` VALUES ('253', '244', 'caption:email:table:page', 'page/caption/email-table.html', 'fa fa-send', '邮箱管理', '邮箱管理', '0', null, '1', '0', '0', '2023-03-14 15:36:09', '2023-03-14 15:36:09');
INSERT INTO `permission` VALUES ('254', '253', 'caption:email:table:add', '', 'fa fa-save', '添加邮箱', '', '1', null, '1', '0', '0', '2023-03-14 15:36:41', '2023-03-14 15:36:41');
INSERT INTO `permission` VALUES ('255', '253', 'caption:email:table:update', '', 'fa fa-save', '修改邮箱', '', '1', null, '1', '0', '0', '2023-03-14 15:37:01', '2023-03-14 15:37:01');
INSERT INTO `permission` VALUES ('256', '253', 'caption:email:table:delete', '', 'fa fa-remove', '删除邮箱', '', '1', null, '1', '0', '0', '2023-03-14 15:37:14', '2023-03-14 15:37:14');
INSERT INTO `permission` VALUES ('257', '253', 'caption:email:table:list', '', 'fa fa-list', '获取用户配置的邮箱列表', '', '1', null, '1', '0', '0', '2023-03-14 15:49:32', '2023-03-14 15:49:32');
INSERT INTO `permission` VALUES ('258', '206', 'admin:file:log:page', 'page/file-log.html', 'fa fa-info', '文件日志', '', '0', null, '1', '0', '0', '2023-03-16 18:27:05', '2023-03-20 09:56:48');
INSERT INTO `permission` VALUES ('259', '231', '3', '3', 'fa ', '3', '', '1', null, '1', '0', '1', '2023-03-19 20:50:41', '2023-03-19 20:50:41');
INSERT INTO `permission` VALUES ('260', '231', 'user:file:record:page', 'page/user/file-record.html', 'fa fa-file', '用户文件上传记录', '用户文件上传记录', '-1', null, '2', '0', '0', '2023-03-19 20:51:33', '2023-03-19 21:13:43');
INSERT INTO `permission` VALUES ('261', '210', 'caption:file:record:page', 'page/caption/file-record.html', 'fa fa-file', '学生文件记录', '查看学生文件相关操作', '-1', null, '1', '0', '0', '2023-03-19 20:58:10', '2023-03-19 20:59:45');
INSERT INTO `permission` VALUES ('262', '206', 'admin:login:log:page', 'page/login-log.html', 'fa fa-info', '登录日志', '登录日志', '0', null, '1', '0', '0', '2023-03-20 09:49:29', '2023-03-20 09:49:29');
INSERT INTO `permission` VALUES ('263', '262', 'admin:login:log:delete', '', 'fa fa-remove', '删除登录日志', '', '1', null, '1', '0', '0', '2023-03-20 09:55:36', '2023-03-20 09:55:36');
INSERT INTO `permission` VALUES ('264', '258', 'admin:file:log:delete', '', 'fa fa-remove', '删除文件日志', '', '1', null, '1', '0', '0', '2023-03-20 09:57:21', '2023-03-20 09:57:21');
INSERT INTO `permission` VALUES ('265', '32', 'admin:notice:page', 'page/notice.html', 'fa fa-edit', '公告管理', '', '-1', null, '3', '0', '0', '2023-03-20 11:39:52', '2023-03-20 11:39:52');
INSERT INTO `permission` VALUES ('266', '265', 'admin:notice:add', '', 'fa fa-save', '发布公告', '', '1', null, '1', '0', '0', '2023-03-20 11:40:14', '2023-03-20 11:40:14');
INSERT INTO `permission` VALUES ('267', '265', 'admin:notice:update', '', 'fa ', '修改公告', '', '1', null, '1', '0', '0', '2023-03-20 11:40:33', '2023-03-20 11:40:33');
INSERT INTO `permission` VALUES ('268', '265', 'admin:notice:delete', '', 'fa fa-remove', '删除公告', '', '1', null, '1', '0', '0', '2023-03-20 11:40:45', '2023-03-20 11:40:45');
INSERT INTO `permission` VALUES ('269', '3', 'admin:darkRoom:user:list', 'page/darkRoom-user.html', 'fa fa-user-times', '小黑屋', '小黑屋中的用户', '0', null, '1', '0', '0', '2023-04-02 15:29:55', '2023-04-02 15:29:55');
INSERT INTO `permission` VALUES ('270', '269', 'admin:darkRoom:user:relieve', '', 'fa fa-info', '解封', '', '1', null, '1', '0', '0', '2023-04-02 15:30:30', '2023-04-02 15:30:30');
INSERT INTO `permission` VALUES ('271', '0', 'common:file:download', '', 'fa fa-cloud-download', '下载文件', '成员以及负责人下载文件', '1', null, '1', '0', '0', '2023-04-05 14:55:15', '2023-04-05 14:55:15');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('6', '超级管理员', '超级管理员');
INSERT INTO `role` VALUES ('12', '团队队长', '团队队长');
INSERT INTO `role` VALUES ('14', '普通用户', '普通用户');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) DEFAULT NULL,
  `r_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4392 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('4280', '1', '6');
INSERT INTO `role_permission` VALUES ('4281', '3', '6');
INSERT INTO `role_permission` VALUES ('4282', '4', '6');
INSERT INTO `role_permission` VALUES ('4283', '90', '6');
INSERT INTO `role_permission` VALUES ('4284', '92', '6');
INSERT INTO `role_permission` VALUES ('4285', '93', '6');
INSERT INTO `role_permission` VALUES ('4286', '107', '6');
INSERT INTO `role_permission` VALUES ('4287', '133', '6');
INSERT INTO `role_permission` VALUES ('4288', '134', '6');
INSERT INTO `role_permission` VALUES ('4289', '269', '6');
INSERT INTO `role_permission` VALUES ('4290', '270', '6');
INSERT INTO `role_permission` VALUES ('4291', '127', '6');
INSERT INTO `role_permission` VALUES ('4292', '128', '6');
INSERT INTO `role_permission` VALUES ('4293', '129', '6');
INSERT INTO `role_permission` VALUES ('4294', '130', '6');
INSERT INTO `role_permission` VALUES ('4295', '131', '6');
INSERT INTO `role_permission` VALUES ('4296', '132', '6');
INSERT INTO `role_permission` VALUES ('4297', '135', '6');
INSERT INTO `role_permission` VALUES ('4298', '138', '6');
INSERT INTO `role_permission` VALUES ('4299', '139', '6');
INSERT INTO `role_permission` VALUES ('4300', '140', '6');
INSERT INTO `role_permission` VALUES ('4301', '235', '6');
INSERT INTO `role_permission` VALUES ('4302', '32', '6');
INSERT INTO `role_permission` VALUES ('4303', '61', '6');
INSERT INTO `role_permission` VALUES ('4304', '62', '6');
INSERT INTO `role_permission` VALUES ('4305', '69', '6');
INSERT INTO `role_permission` VALUES ('4306', '72', '6');
INSERT INTO `role_permission` VALUES ('4307', '234', '6');
INSERT INTO `role_permission` VALUES ('4308', '73', '6');
INSERT INTO `role_permission` VALUES ('4309', '74', '6');
INSERT INTO `role_permission` VALUES ('4310', '75', '6');
INSERT INTO `role_permission` VALUES ('4311', '76', '6');
INSERT INTO `role_permission` VALUES ('4312', '77', '6');
INSERT INTO `role_permission` VALUES ('4313', '78', '6');
INSERT INTO `role_permission` VALUES ('4314', '79', '6');
INSERT INTO `role_permission` VALUES ('4315', '80', '6');
INSERT INTO `role_permission` VALUES ('4316', '81', '6');
INSERT INTO `role_permission` VALUES ('4317', '82', '6');
INSERT INTO `role_permission` VALUES ('4318', '84', '6');
INSERT INTO `role_permission` VALUES ('4319', '85', '6');
INSERT INTO `role_permission` VALUES ('4320', '86', '6');
INSERT INTO `role_permission` VALUES ('4321', '89', '6');
INSERT INTO `role_permission` VALUES ('4322', '87', '6');
INSERT INTO `role_permission` VALUES ('4323', '88', '6');
INSERT INTO `role_permission` VALUES ('4324', '91', '6');
INSERT INTO `role_permission` VALUES ('4325', '206', '6');
INSERT INTO `role_permission` VALUES ('4326', '207', '6');
INSERT INTO `role_permission` VALUES ('4327', '208', '6');
INSERT INTO `role_permission` VALUES ('4328', '209', '6');
INSERT INTO `role_permission` VALUES ('4329', '258', '6');
INSERT INTO `role_permission` VALUES ('4330', '264', '6');
INSERT INTO `role_permission` VALUES ('4331', '262', '6');
INSERT INTO `role_permission` VALUES ('4332', '263', '6');
INSERT INTO `role_permission` VALUES ('4333', '265', '6');
INSERT INTO `role_permission` VALUES ('4334', '266', '6');
INSERT INTO `role_permission` VALUES ('4335', '267', '6');
INSERT INTO `role_permission` VALUES ('4336', '268', '6');
INSERT INTO `role_permission` VALUES ('4337', '197', '6');
INSERT INTO `role_permission` VALUES ('4338', '198', '6');
INSERT INTO `role_permission` VALUES ('4339', '197', '12');
INSERT INTO `role_permission` VALUES ('4340', '198', '12');
INSERT INTO `role_permission` VALUES ('4341', '210', '12');
INSERT INTO `role_permission` VALUES ('4342', '212', '12');
INSERT INTO `role_permission` VALUES ('4343', '213', '12');
INSERT INTO `role_permission` VALUES ('4344', '214', '12');
INSERT INTO `role_permission` VALUES ('4345', '215', '12');
INSERT INTO `role_permission` VALUES ('4346', '216', '12');
INSERT INTO `role_permission` VALUES ('4347', '217', '12');
INSERT INTO `role_permission` VALUES ('4348', '218', '12');
INSERT INTO `role_permission` VALUES ('4349', '219', '12');
INSERT INTO `role_permission` VALUES ('4350', '220', '12');
INSERT INTO `role_permission` VALUES ('4351', '227', '12');
INSERT INTO `role_permission` VALUES ('4352', '221', '12');
INSERT INTO `role_permission` VALUES ('4353', '222', '12');
INSERT INTO `role_permission` VALUES ('4354', '228', '12');
INSERT INTO `role_permission` VALUES ('4355', '229', '12');
INSERT INTO `role_permission` VALUES ('4356', '230', '12');
INSERT INTO `role_permission` VALUES ('4357', '223', '12');
INSERT INTO `role_permission` VALUES ('4358', '224', '12');
INSERT INTO `role_permission` VALUES ('4359', '225', '12');
INSERT INTO `role_permission` VALUES ('4360', '226', '12');
INSERT INTO `role_permission` VALUES ('4361', '239', '12');
INSERT INTO `role_permission` VALUES ('4362', '248', '12');
INSERT INTO `role_permission` VALUES ('4363', '240', '12');
INSERT INTO `role_permission` VALUES ('4364', '241', '12');
INSERT INTO `role_permission` VALUES ('4365', '242', '12');
INSERT INTO `role_permission` VALUES ('4366', '243', '12');
INSERT INTO `role_permission` VALUES ('4367', '244', '12');
INSERT INTO `role_permission` VALUES ('4368', '252', '12');
INSERT INTO `role_permission` VALUES ('4369', '245', '12');
INSERT INTO `role_permission` VALUES ('4370', '246', '12');
INSERT INTO `role_permission` VALUES ('4371', '247', '12');
INSERT INTO `role_permission` VALUES ('4372', '253', '12');
INSERT INTO `role_permission` VALUES ('4373', '254', '12');
INSERT INTO `role_permission` VALUES ('4374', '255', '12');
INSERT INTO `role_permission` VALUES ('4375', '256', '12');
INSERT INTO `role_permission` VALUES ('4376', '257', '12');
INSERT INTO `role_permission` VALUES ('4377', '261', '12');
INSERT INTO `role_permission` VALUES ('4378', '271', '12');
INSERT INTO `role_permission` VALUES ('4379', '197', '14');
INSERT INTO `role_permission` VALUES ('4380', '198', '14');
INSERT INTO `role_permission` VALUES ('4381', '231', '14');
INSERT INTO `role_permission` VALUES ('4382', '232', '14');
INSERT INTO `role_permission` VALUES ('4383', '233', '14');
INSERT INTO `role_permission` VALUES ('4384', '236', '14');
INSERT INTO `role_permission` VALUES ('4385', '237', '14');
INSERT INTO `role_permission` VALUES ('4386', '238', '14');
INSERT INTO `role_permission` VALUES ('4387', '249', '14');
INSERT INTO `role_permission` VALUES ('4388', '250', '14');
INSERT INTO `role_permission` VALUES ('4389', '251', '14');
INSERT INTO `role_permission` VALUES ('4390', '260', '14');
INSERT INTO `role_permission` VALUES ('4391', '271', '14');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) DEFAULT NULL,
  `business_type` tinyint(4) NOT NULL DEFAULT '0',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `content` varchar(255) NOT NULL DEFAULT '',
  `u_id` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(30) NOT NULL DEFAULT '',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `cost_time` int(11) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2135 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `sys_setting`;
CREATE TABLE `sys_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_file_size` int(11) DEFAULT '0',
  `system_file_size` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_setting
-- ----------------------------
INSERT INTO `sys_setting` VALUES ('1', '40');

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `t_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL DEFAULT '0',
  `file_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `is_group` tinyint(4) NOT NULL DEFAULT '0',
  `group_user_limit` int(11) NOT NULL DEFAULT '0',
  `file_name_template` varchar(255) NOT NULL DEFAULT '',
  `task_name_uuid` varchar(100) NOT NULL DEFAULT '',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskNameUuid` (`task_name_uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of task
-- ----------------------------

-- ----------------------------
-- Table structure for task_group
-- ----------------------------
DROP TABLE IF EXISTS `task_group`;
CREATE TABLE `task_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `invite_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of task_group
-- ----------------------------

-- ----------------------------
-- Table structure for task_record
-- ----------------------------
DROP TABLE IF EXISTS `task_record`;
CREATE TABLE `task_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) NOT NULL DEFAULT '0',
  `u_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '',
  `file_path` varchar(255) NOT NULL DEFAULT '',
  `file_name` varchar(100) NOT NULL DEFAULT '',
  `file_size` bigint(11) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `is_submit` tinyint(1) NOT NULL DEFAULT '0',
  `submit_time` datetime DEFAULT NULL,
  `group_user_id` varchar(100) NOT NULL DEFAULT '',
  `troop_id` int(11) DEFAULT '-1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1413 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of task_record
-- ----------------------------

-- ----------------------------
-- Table structure for task_timing_record
-- ----------------------------
DROP TABLE IF EXISTS `task_timing_record`;
CREATE TABLE `task_timing_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `task_name` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `content` longtext,
  `receive` varchar(50) NOT NULL DEFAULT '',
  `file_size` int(11) DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态。0进行中,1发送成功,2发送失败',
  `send_time` datetime DEFAULT NULL,
  `way` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT 0,
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of task_timing_record
-- ----------------------------

-- ----------------------------
-- Table structure for task_user
-- ----------------------------
DROP TABLE IF EXISTS `task_user`;
CREATE TABLE `task_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1397 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of task_user
-- ----------------------------

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `invitation_code` varchar(20) NOT NULL DEFAULT '',
  `u_id` int(11) NOT NULL,
  `file_size_capacity` bigint(20) NOT NULL DEFAULT '500' COMMENT '团队文件大小容量限制',
  `file_size_used` bigint(20) NOT NULL DEFAULT '0' COMMENT '当前已使用过的容量',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of team
-- ----------------------------

-- ----------------------------
-- Table structure for team_user
-- ----------------------------
DROP TABLE IF EXISTS `team_user`;
CREATE TABLE `team_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_id` int(11) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of team_user
-- ----------------------------

-- ----------------------------
-- Table structure for troop
-- ----------------------------
DROP TABLE IF EXISTS `troop`;
CREATE TABLE `troop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of troop
-- ----------------------------

-- ----------------------------
-- Table structure for troop_user
-- ----------------------------
DROP TABLE IF EXISTS `troop_user`;
CREATE TABLE `troop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `troop_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of troop_user
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL DEFAULT '',
  `student_number` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(20) NOT NULL DEFAULT '',
  `notice_state` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(4) DEFAULT '0',
  `gmt_created` datetime DEFAULT NULL,
  `gmt_modify` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_name` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10112 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'xhy', '123', '123456', '11@qq.com', '0', '0', '2023-04-14 21:34:07', null);
INSERT INTO `user` VALUES ('10111', '测试_团队负责人', '123', '1234567', '22@qq.com', '0', '0', '2023-04-14 21:36:48', '2023-04-14 21:36:48');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `r_id` int(255) DEFAULT NULL,
  `u_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('328', '6', '1');
INSERT INTO `user_role` VALUES ('330', '14', '10111');
INSERT INTO `user_role` VALUES ('331', '12', '10111');
